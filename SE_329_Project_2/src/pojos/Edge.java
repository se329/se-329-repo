package pojos;

class Edge{
	protected Course from;
	protected Course to;

	public Edge(Course from, Course to){
		this.from = from;
		this.to = to;
	}

	public Course getFrom(){
		return this.from;
	}

	public void setFrom(Course from){
		this.from = from;
	}

	public Course getTo(){
		return this.to;
	}

	public void setTo(Course to){
		this.to = to;
	}
}
