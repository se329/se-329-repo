package pojos;

import java.util.ArrayList;


public class Degree
{
	private ArrayList<Course> courses = null;
	private ArrayList<Course> electives = null;
	private ArrayList<Course[]> choices = null;

	// CORE CLASSES
	Course chem167 = new Course("CHEM 167", 4);

	Course coms101 = new Course("COM S 101", 0);
	Course coms203 = new Course("COM S 203", 0);
	Course coms227 = new Course("COM S 227", 4);
	Course coms228 = new Course("COM S 228", 3);
	Course coms309 = new Course("COM S 309", 3);
	Course coms311 = new Course("COM S 311", 3);
	Course coms321 = new Course("COM S 321", 3); // Choose between this or crpe 381 for SE
	Course coms327 = new Course("COM S 327", 3); // Choose between this or cpre 288 for SE
	Course coms330 = new Course("COM S 330", 3); // Choose between this or cpre 310 for SE
	Course coms331 = new Course("COM S 331", 3);
	Course coms342 = new Course("COM S 342", 3);
	Course coms352 = new Course("COM S 352", 3); // Choose between this or cpre 308 for SE
	Course coms362 = new Course("COM S 362", 3); // Choose between this or coms 363 for COMS
	Course coms363 = new Course("COM S 363", 3); // Choose between this or coms 362 for COMS

	Course cpre185 = new Course("CPR E 185", 3);
	Course cpre281 = new Course("CPR E 281", 4);
	Course cpre288 = new Course("CPR E 288", 4); // Choose between this or coms 327 for SE
	Course cpre308 = new Course("CPR E 308", 3); // Choose between this or coms 352 for SE
	Course cpre310 = new Course("CPR E 310", 3); // Choose between this or coms 330 for SE
	Course cpre381 = new Course("CPR E 381", 4); // Choose between this or coms 321 for SE
	Course cpre491 = new Course("CPR E 491", 3);
	Course cpre492 = new Course("CPR E 492", 2);

	Course ee201 = new Course("E E 201", 4);
	Course ee230 = new Course("E E 230", 4);

	Course engr101 = new Course("ENGR 101", 0);

	Course engl150 = new Course("ENGL 150", 3);
	Course engl250 = new Course("ENGL 250", 3);
	Course engl309 = new Course("ENGL 309", 3); // Choose between this or engl 314 for SE
	Course engl314 = new Course("ENGL 314", 3); // Choose between this or engl 309 for SE

	Course lib160 = new Course("LIB 160", 1);

	Course math165 = new Course("MATH 165", 4);
	Course math166 = new Course("MATH 166", 4);
	Course math265 = new Course("MATH 265", 4);
	Course math267 = new Course("MATH 267", 4);

	Course phil343 = new Course("PHIL 343", 3);

	Course phys221 = new Course("PHYS 221", 5);
	Course phys222 = new Course("PHYS 222", 5);

	Course se101 = new Course("S E 101", 0);
	Course se166 = new Course("S E 166", 0);
	Course se319 = new Course("S E 319", 3);
	Course se329 = new Course("S E 329", 3);
	Course se339 = new Course("S E 339", 3);
	Course se491 = new Course("S E 491", 3);
	Course se494 = new Course("S E 494", 0);
	Course se492 = new Course("S E 492", 3);

	Course seElective1 = new Course("S E Elective 1", 3);
	Course seElective2 = new Course("S E Elective 2", 3);

	Course seTechElec = new Course("S E Tech Elec", 3);

	Course spcm212 = new Course("SP CM 212", 3);

	Course stat305 = new Course("STAT 305", 3); // Choose between this or stat 330 for SE
	Course stat330 = new Course("STAT 330", 3); // Choose between this or stat 305 for SE
	Course stat341 = new Course("STAT 341", 3); // Choose between this or other 2 stat for COM S

	// COM S ELECTIVES
	Course coms230 = new Course("COM S 230", 3); // Does not count as pre req but required for many
	Course coms252 = new Course("COM S 252", 3);
	Course coms336 = new Course("COM S 336", 3);
	Course coms350 = new Course("COM S 350", 3);
	// Course coms401 = new Course("COM S 401", 3); doesnt exist
	Course coms409 = new Course("COM S 409", 3);
	Course coms412 = new Course("COM S 412", 3);
	Course coms417 = new Course("COM S 417", 3);
	Course coms418 = new Course("COM S 418", 3);
	Course coms421 = new Course("COM S 421", 3);
	Course coms425 = new Course("COM S 425", 3);
	Course coms430 = new Course("COM S 430", 3);
	Course coms437 = new Course("COM S 437", 3);
	Course coms440 = new Course("COM S 440", 3);
	Course coms444 = new Course("COM S 444", 4);
	Course coms454 = new Course("COM S 454", 3);
	Course coms455 = new Course("COM S 455", 3);
	Course coms461 = new Course("COM S 461", 3);
	Course coms472 = new Course("COM S 472", 3);
	Course coms474 = new Course("COM S 474", 3);
	Course coms477 = new Course("COM S 477", 3);
	Course coms481 = new Course("COM S 481", 3);
	Course coms486 = new Course("COM S 486", 3);

	// CPR E ELECTIVES
	Course cpre329 = new Course("CPR E 329", 3);
	Course cpre330 = new Course("CPR E 330", 4);
	Course cpre339 = new Course("CPR E 339", 3);
	Course cpre388 = new Course("CPR E 388", 4);
	Course cpre412 = new Course("CPR E 412", 3);
	Course cpre416 = new Course("CPR E 416", 3);
	// Course cpre418 = new Course("CPR E 418", 4);
	Course cpre419 = new Course("CPR E 419", 4);
	Course cpre425 = new Course("CPR E 425", 3);
	Course cpre431 = new Course("CPR E 431", 3);
	// Course cpre435 = new Course("CPR E 435", 4);
	Course cpre444 = new Course("CPR E 444", 4);
	Course cpre450 = new Course("CPR E 450", 3);
	Course cpre458 = new Course("CPR E 458", 3);
	Course cpre454 = new Course("CPR E 454", 3);
	// Course cpre465 = new Course("CPR E 465", 4);
	Course cpre466 = new Course("CPR E 466", 3);
	Course cpre488 = new Course("CPR E 488", 4);
	Course cpre489 = new Course("CPR E 489", 4);
	Course cpre490 = new Course("CPR E 490", 2);

	// OTHER ELECTIVES
	Course elec300 = new Course("300 Level Elective", 3);
	Course elec400_1 = new Course("400 Level COM S Elective 1", 3);
	Course elec400_2 = new Course("400 Level COM S Elective 2", 3);

	Course arts1 = new Course("Arts and Humanities Elective 1", 3);
	Course arts2 = new Course("Arts and Humanities Elective 2", 3);
	Course arts3 = new Course("Arts and Humanities Elective 3", 3);

	Course econ = new Course("Econ Elective", 3);

	Course eeElec = new Course("E E ELECTIVE", 3);

	Course fl101 = new Course("Foreign Language Elective 1", 4);
	Course fl102 = new Course("Foreign Language Elective 2", 4);

	Course math1 = new Course("Math Elective", 3);

	Course naturalSciences1 = new Course("Natural Science Elective", 3);

	Course opene = new Course("Open Elective 1", 3);
	Course opene2 = new Course("Open Elective 2", 3);

	Course scienceElec = new Course("Science Sequence", 5);

	Course socialScience1 = new Course("Social Science Elective 1", 3);
	Course socialScience2 = new Course("Social Science Elective 2", 3);
	Course socialScience3 = new Course("Social Science Elective 3", 3);

	Course suplmelec1 = new Course("Supplemental Elective 1", 3);
	Course suplmelec2 = new Course("Supplemental Elective 2", 3);
	Course suplmelec3 = new Course("Supplemental Elective 3", 3);

	Course genEd1 = new Course("GEN ED Elective 1", 3);
	Course genEd2 = new Course("GEN ED Elective 2", 3);
	Course genEd3 = new Course("GEN ED Elective 3", 3);
	Course genEd4 = new Course("GEN ED Elective 4", 3);
	Course genEd5 = new Course("GEN ED Elective 5", 3);

	Course techElec1 = new Course("TECH ELECTIVE 1", 3);
	Course techElec2 = new Course("TECH ELECTIVE 2", 3);
	Course techElec3 = new Course("TECH ELECTIVE 3", 3);
	Course cpreElec1 = new Course("CPR E ELECTIVE 1", 3);
	Course cpreElec2 = new Course("CPR E ELECTIVE 2", 3);
	Course comsElec = new Course("COM S ELECTIVE", 3);

	public Degree(String degreeName)
	{
		computePreReqs();
		if (degreeName.toLowerCase().equals("computer engineering"))
		{
			courses = createCPRE();
			choices = new ArrayList<>();
		}
		else if (degreeName.toLowerCase().equals("software engineering"))
		{
			courses = createSE();
			choices = seCourseChoices();
		}
		else if (degreeName.toLowerCase().equals("computer science"))
		{
			courses = createCOMS();
			choices = comsCourseChoices();
		}
		electives = createCPREElectives();
		electives.addAll(createSEElectives());
		electives.addAll(createCOMSElectives());
	}


	public ArrayList<Course> getElectives()
	{
		return electives;
	}

	public ArrayList<Course> getCourses()
	{
		return courses;
	}

	public ArrayList<Course[]> getChoices()
	{
		return choices;
	}

	public void addNewCourse(String courseName)
	{
		for(Course course : electives)
		{
			if(course.getName().equals(courseName))
			{
				courses.add(course);
				break;
			}
		}
	}

	private ArrayList<Course> createCPRE()
	{
		ArrayList<Course> courses = new ArrayList<Course>();
		courses.add(engl150);
		courses.add(engl250);
		courses.add(lib160);
		courses.add(engl314);
		courses.add(chem167);
		courses.add(engr101);
		courses.add(cpre185);
		courses.add(math166);
		courses.add(math165);
		courses.add(phys221);
		courses.add(coms227);
		courses.add(coms228);
		courses.add(math265);
		courses.add(math267);
		courses.add(phys222);
		courses.add(cpre281);
		courses.add(cpre288);
		courses.add(cpre308);
		courses.add(cpre310);
		courses.add(cpre381);
		courses.add(coms311);
		courses.add(ee201);
		courses.add(ee230);
		courses.add(coms327);
		courses.add(coms309);
		courses.add(cpre491);
		courses.add(cpre492);
		courses.add(stat330);
		courses.add(comsElec);
		courses.add(cpreElec1);
		courses.add(cpreElec2);
		courses.add(techElec1);
		courses.add(techElec2);
		courses.add(techElec3);
		courses.add(eeElec);
		courses.add(genEd1);
		courses.add(genEd2);
		courses.add(genEd3);
		courses.add(genEd4);
		courses.add(genEd5);

		return courses;
	}


	private ArrayList<Course> createSE()
	{
		ArrayList<Course> courses = new ArrayList<Course>();
		courses.add(math165);
		courses.add(chem167);
		courses.add(se101);
		courses.add(cpre185);
		courses.add(lib160);
		courses.add(engl150);
		courses.add(math166);
		courses.add(phys221);
		courses.add(coms227);
		courses.add(se166);
		courses.add(econ);
		courses.add(math267);
		courses.add(spcm212);
		courses.add(coms228);
		courses.add(cpre281);
		courses.add(engl250);
		courses.add(arts1);
		courses.add(math1);
		courses.add(cpre288);
		courses.add(coms327);
		courses.add(coms363);
		courses.add(arts2);
		courses.add(se319);
		courses.add(coms309);
		courses.add(cpre310);
		courses.add(coms330);
		courses.add(cpre381);
		courses.add(coms321);
		courses.add(opene);
		courses.add(se339);
		courses.add(se329);
		courses.add(coms311);
		courses.add(cpre308);
		courses.add(coms352);
		courses.add(engl314);
		courses.add(engl309);
		courses.add(socialScience1);
		courses.add(seTechElec);
		courses.add(seElective1);
		courses.add(seElective2);
		courses.add(stat330);
		courses.add(stat305);
		courses.add(se491);
		courses.add(se494);
		courses.add(arts3);
		courses.add(suplmelec1);
		courses.add(suplmelec2);
		courses.add(se492);
		courses.add(suplmelec3);

		return courses;
	}


	private ArrayList<Course> createCOMS()
	{
		ArrayList<Course> courses = new ArrayList<Course>();
		courses.add(coms101);
		courses.add(coms203);
		courses.add(coms227);
		courses.add(coms228);
		courses.add(cpre281);
		courses.add(coms309);
		courses.add(coms311);
		courses.add(coms321);
		courses.add(coms327);
		courses.add(coms331);
		courses.add(coms342);
		courses.add(coms352);
		courses.add(coms362);
		courses.add(coms363);
		courses.add(engl314);
		courses.add(engl309);
		courses.add(math165);
		courses.add(socialScience1);
		courses.add(fl101);
		courses.add(engl150);
		courses.add(math166);
		courses.add(arts1);
		courses.add(fl102);
		courses.add(lib160);
		courses.add(math1);
		courses.add(engl250);
		courses.add(stat305);
		courses.add(stat330);
		courses.add(stat341);
		courses.add(coms330);
		courses.add(naturalSciences1);
		courses.add(opene);
		courses.add(scienceElec);
		courses.add(spcm212);
		courses.add(arts2);
		courses.add(elec300);
		courses.add(socialScience2);
		courses.add(elec400_1);
		courses.add(elec400_2);
		courses.add(phil343);
		courses.add(opene2);
		courses.add(arts3);
		courses.add(socialScience3);

		return courses;
	}


	private ArrayList<Course> createCPREElectives()
	{
		ArrayList<Course> courses = new ArrayList<Course>();
		courses.add(cpre329);
		courses.add(cpre330);
		courses.add(cpre339);
		courses.add(cpre388);
		courses.add(cpre412);
		courses.add(cpre416);
		// courses.add(cpre418);
		courses.add(cpre419);
		courses.add(cpre425);
		courses.add(cpre431);
		// courses.add(cpre435);
		courses.add(cpre444);
		courses.add(cpre450);
		courses.add(cpre458);
		courses.add(cpre454);
		// courses.add(cpre465);
		courses.add(cpre466);
		courses.add(cpre488);
		courses.add(cpre489);
		courses.add(cpre490);
		return courses;
	}


	private ArrayList<Course> createSEElectives()
	{
		ArrayList<Course> courses = new ArrayList<Course>();
		courses.add(coms342);
		courses.add(coms409);
		courses.add(coms412);
		courses.add(cpre416);
		courses.add(coms417);
		courses.add(cpre419);

		return courses;
	}


	private ArrayList<Course> createCOMSElectives()
	{
		ArrayList<Course> courses = new ArrayList<Course>();
		courses.add(coms327);
		courses.add(coms252);
		courses.add(se319);
		courses.add(coms331);
		courses.add(coms336);
		courses.add(coms342);
		courses.add(coms350);
		courses.add(coms362);
		courses.add(coms363);
		// courses.add(coms401); doesnt exist
		courses.add(coms409);
		courses.add(coms412);
		courses.add(coms417);
		courses.add(coms418);
		courses.add(coms421);
		courses.add(coms425);
		courses.add(coms430);
		courses.add(coms437);
		courses.add(coms440);
		courses.add(coms444);
		courses.add(coms454);
		courses.add(coms455);
		courses.add(coms461);
		courses.add(coms472);
		courses.add(coms474);
		courses.add(coms477);
		courses.add(coms481);
		courses.add(coms486);
		return courses;
	}


	private void computePreReqs()
	{
		coms228.addPreReq(coms227);
		coms228.addPreReq(math165);
		coms309.addPreReq(coms228);
		coms311.addPreReq(coms228);
		coms311.addPreReq(math166);
		coms311.addPreReq(engl150);
		coms311.addPreReq(cpre310);
		coms321.addPreReq(coms228);
		coms321.addPreReq(cpre281);
		coms321.addPreReq(engl250);
		coms327.addPreReq(coms228);
		coms327.addPreReq(math166);
		coms330.addPreReq(coms228);
		coms331.addPreReq(coms228);
		coms331.addPreReq(math166);
		coms331.addPreReq(cpre310);
		coms331.addPreReq(engl250);
		coms342.addPreReq(coms228);
		coms342.addPreReq(cpre310);
		coms352.addPreReq(coms321);
		coms352.addPreReq(coms327);
		coms352.addPreReq(engl250);
		coms362.addPreReq(coms228);
		coms362.addPreReq(engl250);
		coms363.addPreReq(coms228);
		coms363.addPreReq(engl250);

		cpre185.addPreReq(math165);
		cpre288.addPreReq(cpre281);
		cpre288.addPreReq(coms227);
		cpre308.addPreReq(cpre381);
		cpre310.addPreReq(coms228);
		cpre381.addPreReq(cpre288);
		cpre491.addPreReq(cpre308);
		cpre491.addPreReq(engl314);
		cpre492.addPreReq(cpre491);

		ee201.addPreReq(math267);
		ee201.addPreReq(phys222);
		ee230.addPreReq(ee201);

		engl250.addPreReq(engl150);
		engl250.addPreReq(lib160);
		engl309.addPreReq(engl250);
		engl314.addPreReq(engl250);

		math166.addPreReq(math165);
		math265.addPreReq(math166);
		math267.addPreReq(math166);

		phil343.addPreReq(socialScience1);

		phys221.addPreReq(math166);
		phys222.addPreReq(phys221);
		phys222.addPreReq(math166);

		se319.addPreReq(coms228);
		se329.addPreReq(coms309);
		se339.addPreReq(se319);
		se491.addPreReq(se329);
		se494.addPreReq(se329);
		se492.addPreReq(se491);
		se492.addPreReq(se494);

		stat305.addPreReq(math165);
		stat330.addPreReq(math166);
		stat341.addPreReq(math265);

		coms230.addPreReq(coms227);
		coms230.addPreReq(math165);
		coms230.addPreReq(engl150);
		coms252.addPreReq(coms227);
		coms336.addPreReq(coms327);
		coms350.addPreReq(coms230);
		coms409.addPreReq(coms309);
		coms412.addPreReq(cpre310);
		coms412.addPreReq(coms311);
		coms412.addPreReq(stat330);
		coms417.addPreReq(coms309);
		coms417.addPreReq(cpre310);
		coms417.addPreReq(engl250);
		coms417.addPreReq(spcm212);
		coms418.addPreReq(coms311);
		coms421.addPreReq(coms230);
		coms425.addPreReq(coms311);
		coms425.addPreReq(coms230);
		coms425.addPreReq(engl250);
		coms425.addPreReq(spcm212);
		coms430.addPreReq(coms311);
		coms430.addPreReq(coms362);
		coms430.addPreReq(engl250);
		coms430.addPreReq(spcm212);
		coms437.addPreReq(coms336);
		coms440.addPreReq(coms331);
		coms440.addPreReq(coms342);
		coms440.addPreReq(engl250);
		coms440.addPreReq(spcm212);
		coms444.addPreReq(math165);
		coms454.addPreReq(coms311);
		coms454.addPreReq(coms352);
		coms455.addPreReq(coms311);
		coms455.addPreReq(coms230);
		coms455.addPreReq(stat330);
		coms455.addPreReq(engl150);
		coms455.addPreReq(spcm212);
		coms461.addPreReq(coms311);
		coms461.addPreReq(engl250);
		coms461.addPreReq(spcm212);
		coms472.addPreReq(coms311);
		coms472.addPreReq(cpre310);
		coms472.addPreReq(stat330);
		coms472.addPreReq(engl250);
		coms472.addPreReq(spcm212);
		coms472.addPreReq(coms342);
		coms474.addPreReq(coms311);
		coms474.addPreReq(cpre310);
		coms474.addPreReq(stat330);
		coms474.addPreReq(math165);
		coms474.addPreReq(engl250);
		coms474.addPreReq(spcm212);
		coms474.addPreReq(coms342);
		coms477.addPreReq(coms228);
		coms477.addPreReq(cpre310);
		coms477.addPreReq(math166);
		coms481.addPreReq(math265);
		coms481.addPreReq(math267);
		coms486.addPreReq(coms352);

		cpre329.addPreReq(coms309);
		cpre330.addPreReq(ee201);
		cpre330.addPreReq(ee230);
		cpre330.addPreReq(cpre281);
		cpre339.addPreReq(se319); // might be cpre319
		cpre388.addPreReq(cpre288);
		cpre412.addPreReq(cpre310);
		cpre412.addPreReq(coms311);
		cpre412.addPreReq(stat330);
		cpre416.addPreReq(coms309);
		// cpre418.addPreReq(ee230);
		// cpre418.addPreReq(ee311);
		cpre419.addPreReq(cpre308);
		cpre419.addPreReq(coms309);
		cpre425.addPreReq(coms311);
		cpre425.addPreReq(coms330);
		cpre431.addPreReq(cpre489);
		// cpre435.addPreReq(ee324);
		// cpre435.addPreReq(ee324);
		// cpre435.addPreReq(ee330);
		// cpre435.addPreReq(ee322);
		cpre444.addPreReq(math165);
		cpre450.addPreReq(cpre308);
		cpre458.addPreReq(cpre308);
		cpre454.addPreReq(coms311);
		cpre454.addPreReq(coms352);
		// cpre465.addPreReq(ee330);
		// cpre466; //we should take this one out, requires senior classification + instructior permission
		// how do we add that?
		cpre488.addPreReq(cpre381);
		cpre488.addPreReq(coms321);
		cpre489.addPreReq(cpre381);
		// cpre490; //same as 466
	}


	private ArrayList<Course[]> seCourseChoices()
	{
		// change strings to objects for object comparison
		ArrayList<Course[]> seChoices = new ArrayList<Course[]>();
		Course choices[];
		choices = new Course[2];
		choices[0] = coms327;
		choices[1] = cpre288;
		seChoices.add(choices);
		choices = new Course[2];
		choices[0] = engl314;
		choices[1] = engl309;
		seChoices.add(choices);
		choices = new Course[2];
		choices[0] = cpre310;
		choices[1] = coms330;
		seChoices.add(choices);
		choices = new Course[2];
		choices[0] = stat330;
		choices[1] = stat305;
		seChoices.add(choices);
		choices = new Course[2];
		choices[0] = cpre381;
		choices[1] = coms321;
		seChoices.add(choices);
		choices = new Course[2];
		choices[0] = cpre308;
		choices[1] = coms352;
		seChoices.add(choices);
		return seChoices;
	}


	private ArrayList<Course[]> comsCourseChoices()
	{
		// change strings to objects for object comparison
		ArrayList<Course[]> comsChoices = new ArrayList<Course[]>();
		Course[] choices = new Course[2];
		choices[0] = coms362;
		choices[1] = coms363;
		comsChoices.add(choices);
		choices = new Course[2];
		choices[0] = stat305;
		choices[1] = stat330;
		comsChoices.add(choices);
		choices = new Course[2];
		choices[0] = engl314;
		choices[1] = engl309;
		comsChoices.add(choices);
		return comsChoices;
	}


	public void deleteCourse(String courseName)
	{
		for (Course currentCourseCheck : courses)
		{
			if ((currentCourseCheck).getName().equals(courseName))
			{
				deletePreReqa(currentCourseCheck);
				courses.remove(currentCourseCheck);
				break;
			}
		}
	}


	private void deletePreReqa(Course currentCourseCheck) 
	{
		for(Course course : courses)
		{
			for(Course preReq : course.getPreReqsList())
				if(preReq.getName().equals(currentCourseCheck.getName()))
				{
					course.removePreReq(preReq);
					break;
				}
		}
	}
}
