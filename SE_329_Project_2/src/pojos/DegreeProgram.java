package pojos;

import java.util.ArrayList;

/*
 * WHEN PUSHING CLASSES BACK A SEMESTER DUE TO CREDIT LIMITATION:
 * Need to increment the "semesterNumber" by 1 for all courses 
 * pushed back a semester AND for all courses that can be reached
 * by each original course pushed back. Best way to do this is 
 * probably some variant of BFS (DO NOT REVISIT NODES).
 * 
 * TECH ELECTIVE ISSUE:
 * Currently all electives are listed as some of the first courses
 * to be taken, this is because they currently have no pre-reqs 
 * due to the fact that the actual courses are unknown. The solution
 * is to either remove all electives from the topological sort 
 * and then add them to the appropriate semesters after the sort 
 * is completed, or to have the user select all of their electives
 * before the sort runs. I am in favor of the second option because 
 * it will make planning each individual semester easier and it 
 * will give the user a more accurate schedule that they wont need 
 * to edit later. 
 */

public class DegreeProgram{

	ArrayList<Course> courses = new ArrayList<Course>();
	ArrayList<Edge> edges = new ArrayList<Edge>();

	public DegreeProgram(Degree degree){
		this.courses = degree.getCourses();
		for (int i = 0; i<courses.size(); i++){
			Course c = courses.get(i);
			for (int j = 0; j<c.getReqiredForList().size(); j++){
				Course d = c.getReqiredForList().get(j);
				addEdge(c, d);
			}
		}
	}

	public Course getCourse(int index){
		return courses.get(index);
	}

	public Edge getEdge(int index){
		return edges.get(index);
	}

	public void addCourse(Course c){
		courses.add(c);
	}

	public void addEdge(Course from, Course to){
		Edge e = new Edge(from, to);
		edges.add(e);
	}

	public void removeCourse(Course c){
		courses.remove(c);
	}

	public void removeEdge(Edge e){
		e.getFrom().removeRequiredFor(e.getTo());
		edges.remove(e);
	}

	public ArrayList<Course> getCoursesList(){
		return courses;
	}

	public ArrayList<Edge> getEdgesList(){
		return edges;
	}

	public void setCoursesList(ArrayList<Course> courses){
		this.courses = courses;
	}

	public void setEdgesList(ArrayList<Edge> edges){
		this.edges = edges;
	}

	public void BFS(Course root){
		for (int i = 0; i<courses.size(); i++){
			Course c = courses.get(i);
			c.setIsPushedBack(false);
		}
		ArrayList<Course> q = new ArrayList<Course>();
		root.pushBack();
		q.add(root);
		while (!q.isEmpty()){
			Course current = q.get(0);
			for (int j = 0; j <current.getReqiredForList().size(); j++){
				Course next = current.getReqiredForList().get(j);
				if((next.getIsPushedBack() == false) && (next.getSemesterNumber() <= current.getSemesterNumber())){
					next.pushBack();
					q.add(next);
				}
			}
			q.remove(current);
		}
	}

	/**
	Implementation of Kahn's Algorithm
	 **/
	public ArrayList<Course> topologicalSort(ArrayList<Course> coursesWeCareAbout){
		//TODO look into see necessary line
		ArrayList<Course> topo = new ArrayList<Course>();
		ArrayList<Course> tempCourses = new ArrayList<Course>();
		ArrayList<Edge> tempEdges = edges;

		tempCourses = getNodesWithNoInDegree();
		while (!tempCourses.isEmpty()){
			Course current = tempCourses.remove(0);
			int currentSemester = current.getSemesterNumber();
			topo.add(current);
			for (Course newCourse : current.getReqiredForList()){	
				Edge e = findEdge(current, newCourse);
				tempEdges.remove(e);
				/*I think the next line is still necessary but we need to make
				 *sure that we are not decrementing the in degree twice
				 */
				newCourse.setInDegree(newCourse.getInDegree()-1);		//Still necessary?
				if (newCourse.getInDegree() == 0 && coursesWeCareAbout.contains(newCourse)){
					newCourse.setSemesterNumber(currentSemester+1);
					newCourse.setLatestPreReq(current);
					tempCourses.add(newCourse);
				}
			}
		}

		return topo;

	}

	/**
	Returns a list of all courses that have an in degree of 0 AKA no prerequisites
	 **/
	public ArrayList<Course> getNodesWithNoInDegree(){
		ArrayList<Course> noInDegree = new ArrayList<Course>();
		for (Course c : this.courses){
			if (c.getInDegree() == 0){
				noInDegree.add(c);
			}
		}
		return noInDegree;
	}

	/**
	Finds a specfic edge in the edges list given a "from" node and a "to" node
	 **/
	public Edge findEdge(Course from, Course to){
		for (Edge e : this.edges){
			if (e.getFrom().equals(from)){
				if (e.getTo().equals(to)){
					return e;
				}
			}
		}
		return null;
	}

	public void printList(ArrayList<Course> list){
		int currentSem = 1;
		for (Course c : list){
			if(c.getSemesterNumber() > currentSem){
				currentSem = c.getSemesterNumber();
				System.out.println();
			}
			String latest = "NONE";
			if(c.getLatestPreReq() != null){
				latest = c.getLatestPreReq().getName();
			}
			System.out.println(c.getName() + " -  Semester: " + c.getSemesterNumber() + " - Latest Pre-Req: " + latest);
		}
	}

	public ArrayList<String> showCollegeSchedule(ArrayList<Course> list, int desiredCreditsPerSemester){
		ArrayList<String> finalOut = new ArrayList();
		int currentSem = 1;
		int creditsThisSemester = 0;
		for (int i=0; i<list.size(); i++){
			Course c = list.get(i);
			if (creditsThisSemester+c.getCredits() > desiredCreditsPerSemester){
				finalOut.add("Semester " + currentSem + " total credits: " + creditsThisSemester);
				currentSem++;
				finalOut.add("");
				for (int j=i; j<list.size(); j++){
					Course d = list.get(j);
					if(d.getSemesterNumber() < currentSem){
						BFS(d);
					}
				}
				creditsThisSemester = 0;
			}
			if(c.getSemesterNumber() > currentSem){
				finalOut.add("Semester " + currentSem + " total credits: " + creditsThisSemester);
				currentSem = c.getSemesterNumber();
				creditsThisSemester = 0;
				finalOut.add("");
			}
			String latest = "NONE";
			if(c.getLatestPreReq() != null){
				latest = c.getLatestPreReq().getName();
			}
			creditsThisSemester += c.getCredits();
			finalOut.add(c.getName() + " -  Semester: " + c.getSemesterNumber() + " - Latest Pre-Req: " + latest);
		}
		finalOut.add("Semester " + currentSem + " total credits: " + creditsThisSemester);
		return finalOut;
	}
}

