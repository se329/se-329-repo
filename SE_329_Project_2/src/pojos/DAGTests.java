package pojos;

import java.util.ArrayList;

public class DAGTests {
	public static void main(String[] args){
				
		DegreeProgram ComputerEngineering = new DegreeProgram(new Degree("computer engineering"));
		
		System.out.println("Running topo sort...");
		ArrayList<Course> topo = ComputerEngineering.topologicalSort(null);
		for( String course : ComputerEngineering.showCollegeSchedule(topo, 16))
			System.out.println(course);
	}
	
	public static void printCurrentCourses(ArrayList<Course> courses){
		for (Course c : courses){
			System.out.println("Course " + c.getName() + " has " + c.getPreReqsList().size() + " pre-req(s):");
			for (Course d : c.getPreReqsList()){
				System.out.print(d.getName() + ", ");
			}
			System.out.println();
			System.out.println("Course " + c.getName() + " is required for  " + c.getReqiredForList().size() + " course(s):");
			for (Course d : c.getReqiredForList()){
				System.out.print(d.getName() + ", ");
			}
			System.out.println();
			System.out.println("------------------------------------------");
			System.out.println();
		}
		
	}
}
