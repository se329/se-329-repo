package pojos;

import java.util.ArrayList;

public class Course{
	private ArrayList<Course> preReqs = new ArrayList<Course>();
	private ArrayList<Course> requiredFor = new ArrayList<Course>();
	private ArrayList<Edge> edges = new ArrayList<Edge>();
	private String courseName;
	private int inDegree;
	private int outDegree;
	private int credits;
	private int semesterNumber;
	private boolean isPushedBack;
	private Course parent;

	public Course(String name, int credits){
		this.courseName = name;
		this.inDegree = 0;
		this.outDegree = 0;
		this.credits = credits;
		this.semesterNumber = 1;
		this.parent = null;
	}
	
	public Course(String name, int credits, ArrayList<Course> preReqs, ArrayList<Course> requiredFor){
		this.preReqs = preReqs;
		this.requiredFor = requiredFor;
		this.courseName = name;
		this.inDegree = preReqs.size();
		this.outDegree = requiredFor.size();
		this.credits = credits;
		this.semesterNumber = 1;
		this.parent = null;
	}
	
	public ArrayList<Course> getPreReqsList(){
		return this.preReqs;
	}

	public void setPreReqsList(ArrayList<Course> list){
		this.preReqs = list;
	}

	public ArrayList<Course> getReqiredForList(){
		return this.requiredFor;
	}

	public void setReqiredForList(ArrayList<Course> list){
		this.requiredFor = list;
	}

	public ArrayList<Edge> getEdgesList(){
		return this.edges;
	}

	public void setEdgesList(ArrayList<Edge> list){
		this.edges = list;
	}

	public String getName(){
		return this.courseName;
	}

	public void setName(String name){
		this.courseName = name;
	}

	public int getInDegree(){
		return this.inDegree;
	}

	public void setInDegree(int degree){
		this.inDegree = degree;
	}

	public int getOutDegree(){
		return this.outDegree;
	}

	public void setOutDegree(int degree){
		this.outDegree = degree;
	}
	
	public int getCredits(){
		return this.credits;
	}
	
	public void setCredits(int credits){
		this.credits = credits;
	}
	
	public int getSemesterNumber(){
		return this.semesterNumber;
	}
	
	public void setSemesterNumber(int num){
		this.semesterNumber = num;
	}
	
	public Course getLatestPreReq(){
		return this.parent;
	}
	
	public void setLatestPreReq(Course c){
		this.parent = c;
	}
	
	public boolean getIsPushedBack(){
		return this.isPushedBack;
	}
	
	public void setIsPushedBack(boolean pushedBack){
		this.isPushedBack = pushedBack;
	}
	
	public void pushBack(){
		this.semesterNumber++;
	}

	public void addPreReq(Course req){
		this.inDegree++;
		int degree = req.getOutDegree();
		req.setOutDegree(degree+1);
		preReqs.add(req);
		req.addReqiredFor(this);
	}

	public void removePreReq(Course req){
		this.inDegree--;
		int degree = req.getOutDegree();
		req.setOutDegree(degree-1);
		preReqs.remove(req);
		req.removeRequiredFor(this);
	}

	public void addReqiredFor(Course req){
		requiredFor.add(req);
	}

	public void removeRequiredFor(Course req){
		requiredFor.remove(req);
	}
}