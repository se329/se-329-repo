package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import pojos.Degree;


public class RunGUI extends JFrame
{
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	private JTextField nameTextField;
	private JTextField creditsSemesterTextField;
	private JComboBox degreeComboBox;

	private String[] degrees = { "Computer Engineering", "Software Engineering", "Computer Science"};


	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					RunGUI frame = new RunGUI();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}


	/**
	 * Create the frame.
	 * @throws FileNotFoundException 
	 */
	public RunGUI() throws FileNotFoundException
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 258);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		getContentPane().setLayout(null);
		setName("Welcome!");

		JLabel titleLabel = new JLabel("Schedule Planner");
		titleLabel.setFont(new Font("Tahoma", Font.BOLD, 30));
		titleLabel.setBounds(21, 23, 262, 28);
		getContentPane().add(titleLabel);

		JLabel nameLabel = new JLabel("Name:");
		nameLabel.setFont(new Font("Tahoma", Font.BOLD, 17));
		nameLabel.setBounds(21, 73, 55, 28);
		getContentPane().add(nameLabel);

		nameTextField = new JTextField();
		nameTextField.setBounds(199, 80, 166, 20);
		getContentPane().add(nameTextField);
		nameTextField.setColumns(10);

		JLabel degreeLabel = new JLabel("Select Degree:");
		degreeLabel.setFont(new Font("Tahoma", Font.BOLD, 17));
		degreeLabel.setBounds(21, 113, 124, 28);
		getContentPane().add(degreeLabel);

		degreeComboBox = new JComboBox(degrees);
		degreeComboBox.setBounds(199, 120, 166, 20);
		getContentPane().add(degreeComboBox);

		JLabel creditsSemesterLabel = new JLabel("Credits/Semester");
		creditsSemesterLabel.setFont(new Font("Tahoma", Font.BOLD, 17));
		creditsSemesterLabel.setBounds(21, 153, 150, 28);
		getContentPane().add(creditsSemesterLabel);

		creditsSemesterTextField = new JTextField();
		creditsSemesterTextField.setColumns(10);
		creditsSemesterTextField.setBounds(199, 160, 55, 20);
		getContentPane().add(creditsSemesterTextField);

		JButton okButton = new JButton("Ok");
		okButton.setBounds(329, 173, 71, 23);
		okButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent evt)
			{
				if (getName().equals("") || getCreditsSemester() < 0 || getCreditsSemester() > 19)
				{
					JOptionPane.showMessageDialog(getContentPane(), "You did not enter data correctly in the fields.", "Input Error", JOptionPane.ERROR_MESSAGE);
				}
				else
				{
					try
					{
						setVisible(false);
						new ClassesTaken(new Degree(getSelectedDegree()), Integer.parseInt(creditsSemesterTextField.getText()), nameTextField.getText()).setVisible(true);
					}
					catch (FileNotFoundException e)
					{
						e.printStackTrace();
					}
				}
			}
		});
		getContentPane().add(okButton);

	}


	public int getCreditsSemester()
	{
		try
		{
			return Integer.parseInt(creditsSemesterTextField.getText());
		}
		catch (java.lang.NumberFormatException e)
		{
			return -1;
		}
	}


	public String getName()
	{
		return nameTextField.getText();
	}


	public String getSelectedDegree()
	{
		return degreeComboBox.getSelectedItem().toString();
	}
}
