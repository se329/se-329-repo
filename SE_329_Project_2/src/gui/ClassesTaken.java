package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import pojos.Course;
import pojos.Degree;
import pojos.DegreeProgram;


public class ClassesTaken extends JFrame
{
	private static final long serialVersionUID = 1L;

	public JList selectedCourse;
	private JPanel contentPane;


	/**
	 * Create the panel.
	 * @param degree 
	 * @param credits 
	 * @param name 
	 * @throws FileNotFoundException 
	 */
	public ClassesTaken(Degree degree, int credits, String name) throws FileNotFoundException
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 648, 502);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		getContentPane().setLayout(null);
		setName("Select Classes that you have taken");
		contentPane.setLayout(null);

		//Courses jlabel
		JLabel lblCourses = new JLabel("Courses");
		lblCourses.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblCourses.setBounds(34, 11, 68, 28);
		getContentPane().add(lblCourses);

		//JPanel for the availble courses to be displayed
		JPanel availableCoursesPanel = new JPanel(new BorderLayout());
		availableCoursesPanel.setBounds(34, 50, 257, 338);
		getContentPane().add(availableCoursesPanel);

		//Set up the datamodel for the classes
		DataModel availableCourses = new DataModel(degree.getCourses());
		JList<String> courses = new JList<String>(availableCourses);
		courses.setCellRenderer(new SelectRenderer());
		courses.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				if (!e.getValueIsAdjusting())
				{
					//Whenever a class is selected, set this variable to that course name
					selectedCourse = (JList) e.getSource();
				}
			}
		});
		//Add the list into a jscroll pane
		JScrollPane availableCoursesSCrollPane = new JScrollPane(courses);
		availableCoursesPanel.add(availableCoursesSCrollPane, BorderLayout.CENTER);

		//JPanel for the courses taken to go
		JPanel coursesTakenPanel = new JPanel(new BorderLayout());
		coursesTakenPanel.setBounds(335, 50, 257, 338);
		getContentPane().add(coursesTakenPanel);

		//Set up the data model for all classes taken
		DataModel coursesTakenDatamodel = new DataModel();
		JList<String> coursesTakenList = new JList<String>(coursesTakenDatamodel);
		coursesTakenList.setCellRenderer(new SelectRenderer());
		coursesTakenList.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				if (!e.getValueIsAdjusting())
				{
					selectedCourse = (JList) e.getSource();
				}
			}
		});
		//Add it to a jscroll pane
		JScrollPane selectedCoursesScrollPane = new JScrollPane(coursesTakenList);
		coursesTakenPanel.add(selectedCoursesScrollPane, BorderLayout.CENTER);

		//Add in the remove button
		JButton removeCourseButton = new JButton("Remove");
		removeCourseButton.setBounds(426, 399, 89, 23);
		removeCourseButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent evt)
			{
				if (coursesTakenDatamodel.getSize() == 1)
				{
					String toAddToAvailable = coursesTakenDatamodel.clearListGetLast();
					availableCourses.addElement(toAddToAvailable);
				}
				else if (selectedCourse != null && selectedCourse.getSelectedValue() != null)
				{
					availableCourses.addElement(selectedCourse.getSelectedValue().toString());
					coursesTakenDatamodel.removeElement(selectedCourse.getSelectedValue().toString());
				}
			}
		});
		getContentPane().add(removeCourseButton);

		//Add in an add button
		JButton addCourseButton = new JButton("Add");
		addCourseButton.setBounds(117, 399, 89, 23);
		addCourseButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent evt)
			{
				if (availableCourses.getSize() == 1)
				{
					String toAddToTaken = availableCourses.clearListGetLast();
					coursesTakenDatamodel.addElement(toAddToTaken);
				}
				else if (selectedCourse != null && selectedCourse.getSelectedValue() != null)
				{
					coursesTakenDatamodel.addElement(selectedCourse.getSelectedValue().toString());
					availableCourses.removeElement(selectedCourse.getSelectedValue().toString());
				}
			}
		});
		getContentPane().add(addCourseButton);

		//Course taken jlabel
		JLabel coursesTakenLabel = new JLabel("Courses Taken");
		coursesTakenLabel.setFont(new Font("Tahoma", Font.BOLD, 17));
		coursesTakenLabel.setBounds(335, 11, 140, 28);
		getContentPane().add(coursesTakenLabel);

		//Generate schedule button
		JButton createScheduleButton = new JButton("Create Schedule");
		createScheduleButton.setBounds(250, 429, 129, 23);
		createScheduleButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent evt)
			{
				//Set up drop menu full of electives
				DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
				
				for(Course c : degree.getElectives())
					model.addElement(c.getName());
				
				

				//Loop through available courses and find all tech electives and make the user choose which one they want to take
				ArrayList<String> selectedElectives = new ArrayList<>();
				ArrayList<String> toDelete = new ArrayList<>();
				
				for(String courseName : availableCourses.getDataList())
					if(courseName.contains("Supplemental Elective") || courseName.contains("S E Elective") 
							|| courseName.contains("S E Tech Elec") || courseName.contains("400 Level COM S Elective") 
							|| courseName.contains("TECH ELECTIVE") || courseName.contains("CPR E ELECTIVE") 
							|| courseName.contains("COM S ELECTIVE"))
					{
						JPanel dropDownMenuPanel = new JPanel();
						dropDownMenuPanel.add(new JLabel("Select your planned course for your " + courseName));
						
						JComboBox comboBox = new JComboBox(model);
						dropDownMenuPanel.add(comboBox);
						
						JOptionPane.showConfirmDialog(ClassesTaken.this, dropDownMenuPanel, "Electives", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE);
						selectedElectives.add(comboBox.getSelectedItem().toString());
						toDelete.add(courseName);
						model.removeElement(comboBox.getSelectedItem().toString());
					}
				
				availableCourses.getDataList().addAll(selectedElectives);
				availableCourses.getDataList().removeAll(selectedElectives);

				//First delete all classes taken from the program
				for(String courseName : coursesTakenDatamodel.getDataList())
					degree.deleteCourse(courseName);
				
				for(String courseName : toDelete)
					degree.deleteCourse(courseName);
				
				for(String courseName : selectedElectives)
					degree.addNewCourse(courseName);

				//Set up an arraylist to capture all of the classes chosen from the paris
				ArrayList<String> selectedClass = new ArrayList<>();

				for(Course[] pair : degree.getChoices())
				{
					if(checkIfPairExistsInAvailableCourses(availableCourses.getDataList(), pair))
					{
						//Prompt for what class from the pair they want to take
						JCheckBox class1 = new JCheckBox(pair[0].getName());
						JCheckBox class2 = new JCheckBox(pair[1].getName());
						class1.setSelected(true);
						class1.addActionListener(new ActionListener()
						{
							public void actionPerformed(ActionEvent evt)
							{
								if (class1.isSelected())
									class2.setSelected(false);
								else
									class2.setSelected(true);
							}
						});
						class2.addActionListener(new ActionListener()
						{
							public void actionPerformed(ActionEvent evt)
							{
								if (class2.isSelected())
									class1.setSelected(false);
								else
									class1.setSelected(true);
							}
						});
						String message = "Pick a class from the 'Either/Or' class pairs";
						Object[] params = { message, class1, class2 };
						JOptionPane.showConfirmDialog(ClassesTaken.this, params, "Choose a class", JOptionPane.DEFAULT_OPTION);

						//Get the selected class
						if(class1.isSelected())
							selectedClass.add(pair[1].getName());
						else
							selectedClass.add(pair[0].getName());
					}
				}

				for(String courseName : selectedClass)
					degree.deleteCourse(courseName);

				setVisible(false);
				new DisplaySchedule(new DegreeProgram(degree), credits, name).setVisible(true);
			}
		});
		getContentPane().add(createScheduleButton);
	}


	protected boolean checkIfPairExistsInAvailableCourses(ArrayList<String> dataList, Course[] pair)
	{
		for(Course course : pair)
		{
			boolean found = false;
			for(String availableCourse : dataList)
			{
				if(course.getName().equals(availableCourse))
				{
					found = true;
					break;
				}
			}
			if(!found)
				return false;
		}
		return true;
	}


	public class DataModel extends AbstractListModel<String>
	{
		private static final long serialVersionUID = 1L;

		public ArrayList<String> dataList;

		public ArrayList<String> getDataList()
		{
			return dataList;
		}

		public DataModel(ArrayList<Course> arrayList) throws FileNotFoundException
		{
			dataList = new ArrayList<String>();
			for (Course c : arrayList)
			{
				dataList.add(c.getName());
			}
		}


		public DataModel() throws FileNotFoundException
		{
			dataList = new ArrayList<String>();
		}


		@Override
		public int getSize()
		{
			return dataList.size();
		}


		@Override
		public String getElementAt(int index)
		{
			return dataList.get(index);
		}


		public String clearListGetLast()
		{
			String toReturn = dataList.get(0);
			dataList = new ArrayList<String>();
			fireContentsChanged(this, 0, dataList.size() - 1);
			return toReturn;
		}


		public void addElement(String s)
		{
			if (!dataList.contains(s) && s != null)
			{
				dataList.add(s);
				fireIntervalAdded(this, dataList.size() + 1, dataList.size() + 1);
			}
		}


		public void removeElement(String toDelete)
		{
			if (dataList.contains(toDelete) && toDelete != null)
			{
				dataList.remove(toDelete);

				if (dataList.size() != 0)
					fireIntervalRemoved(this, dataList.size() - 1, dataList.size() - 1);
				else
					fireIntervalRemoved(this, dataList.size(), dataList.size());
			}
		}
	}

	class SelectRenderer implements ListCellRenderer<Object>
	{
		@Override
		public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus)
		{

			JLabel label = new JLabel((String) value);
			label.setOpaque(true);

			if (isSelected)
			{
				label.setBackground(Color.yellow);
			}
			else
			{
				label.setBackground(Color.white);
			}
			return label;
		}
	}
}
