package gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import pojos.DegreeProgram;

public class DisplaySchedule extends JFrame {
	/**
	 * This will display the schedule that is generated.
	 */
	private JPanel contentPane;
	/**
	 * Create the frame.
	 * @param name 
	 */
	public DisplaySchedule(DegreeProgram degree, int credits, String name) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setTitle("Planned Schedule for: " + name);
		contentPane.setLayout(new GridLayout(0, 1));

		DefaultListModel<String> list = new DefaultListModel<String>();
		JList<String> displayArea = new JList<String>(list);
		displayArea.setBounds(0, 0, 434, 261);
		contentPane.add(displayArea);
		ArrayList<String> out = degree.showCollegeSchedule(degree.topologicalSort(degree.getCoursesList()), credits);

		JScrollPane sp = new JScrollPane(displayArea);
		contentPane.add(sp, BorderLayout.CENTER);

		for(String course : out)
		{
			list.addElement(course);
			if(course.contains("total"))
				list.addElement(" ");
		}
	}
}
