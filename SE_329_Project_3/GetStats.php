<?php

$users = json_decode(file_get_contents("users.json"), true);

function getStats($users){
	$data = '';
	for($i = 0; $i < sizeof($users); $i++){
		$data = $data . 'User: ' . $users[$i]['username'] . '<br>'
			. 'Easy Score: ' . $users[$i]['easyHS'] . '<br>'
			. 'Medium Score: ' . $users[$i]['mediumHS'] . '<br>'
			. 'Hard Score: ' . $users[$i]['hardHS'] . '<br><br>';			
	}
	return $data;
}
echo json_encode(getStats($users));
?>