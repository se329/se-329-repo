
//Put JS here upon setup of HTML
//Testing this here.
var points = 15;
var input = 0;
var solution = 0;
var beginButton = $("#begin");
var textBox = document.getElementById("answer");
var wantsToQuit = false;
var diff;
var hasAddition;
var hasSubtraction;
var hasMultiplication;
var hasDivision;

$(document).ready(function(){
	var array = JSON.parse(localStorage.getItem('settings'));
	if(array[0] == "Easy"){
		diff = 5;
	}
	else if(array[0] == "Medium"){
		diff = 10;
	}
	else if(array[0] == "Hard"){
		diff = 15;
	}
	hasAddition = array[1];
	hasSubtraction = array[2];
	hasMultiplication = array[3];
	hasDivision = array[4];
});

function initialize(difficulty, hasAddition, hasSubtraction, hasMultiplication, hasDivision){
	hideBeginingStuff();
	mathThing(difficulty, hasAddition, hasSubtraction, hasMultiplication, hasDivision);
}

function quitTheGame(){
	window.location = 'Menu.html';
}

function updateUserAndQuit(){
	console.log(JSON.parse(localStorage.getItem('currentUser')));
	var username = JSON.parse(localStorage.getItem('currentUser'));
		$.post("UpdateScore.php", {username : username, diff : diff, points : points},
			function(data,status){
				if(data == "true"){
					alert("Your score was " + points + "! Good job!")
					window.location = 'Menu.html';
				}
				})
}

function hideBeginingStuff(){
	document.getElementById("begin").style.display = "none";
	document.getElementById("instructions").style.display = "none";
	document.getElementById("submit").style.display = "inline";
	document.getElementById("answer").style.display = "block";
	document.getElementById("score").style.display = "block";
	document.getElementById("label").style.display = "block";
	document.getElementById("quit").style.display = "inline";
}

function addition(difficulty){
	var x = Math.floor((Math.random() * difficulty)+1);					//Create 2 random values
	var y = Math.floor((Math.random() * difficulty)+1);
	$("#question").text("What is the sum of " + x + " and " + y + "?");
	return x + y;														//Return solution
}

function subtraction(difficulty){
	var x = Math.floor((Math.random() * difficulty)+1);
	var y = Math.floor((Math.random() * difficulty)+1);
	
	$("#question").text("What is the difference between " + x + " and " + y + "?");

	return Math.abs(x - y);
}

function multiplication(difficulty){
	var x = Math.floor((Math.random() * difficulty)+1);
	var y = Math.floor((Math.random() * difficulty)+1);
	$("#question").text("What is the product of " + x + " and " + y + "?");
	return x * y;
}

function division(difficulty){
	var x = Math.floor((Math.random() * difficulty)+1);
	var y = Math.floor((Math.random() * difficulty)+1);
	var z = x * y;
	$("#question").text("What is the quotient of " + z + " by " + y + "?");
	return x;
}

function mathThing(difficulty, hasAddition, hasSubtraction, hasMultiplication, hasDivision){
	console.log(points);
	input = Math.abs(document.getElementById('answer').value);
	
	document.getElementById('mainForm').setAttribute( "autocomplete", "off" );
	
	document.getElementById('answer').value = "";

	console.log("input is: " + input);
	
	if (input == solution){
		points += difficulty;
		console.log("Good Job!");
	}
	else{
		points -= difficulty;
		console.log("You SUCK!!!");
	}
	if (points >= 0 && !wantsToQuit){
		$("#score").text("Your Score is: " + points);
		while (true)
		{
			var problemType = Math.floor((Math.random() * 3)+1);
			
			if (problemType == 1 && hasAddition){
				solution = addition(difficulty);
				break;
			}
			if (problemType == 2 && hasSubtraction){
				solution = subtraction(difficulty);
				break;
			}
			if (problemType == 3 && hasMultiplication){
				solution = multiplication(difficulty);
				break;
			}
			if (problemType == 4 && hasDivision){
				solution = division(difficulty);
				break;
			}
		}
	}
	else{
		$("#score").text("Game Over!");
		alert("You lose!");
		window.location = 'Menu.html';
		console.log("Game over!");
	}
	if (wantsToQuit){
		window.location = "Menu.html";
	}
}