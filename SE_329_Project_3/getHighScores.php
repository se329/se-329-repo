<?php

$users = json_decode(file_get_contents("users.json"), true);
$easyHS = 0;
$mediumHS = 0;
$hardHS = 0;

$toReturn = array(0,0,0);

for($i = 0; $i < sizeof($users); $i++){
	if($users[$i]['easyHS'] > $easyHS){
		$easyHS = $users[$i]['easyHS'];
	}
	if($users[$i]['mediumHS'] > $mediumHS){
		$mediumHS = $users[$i]['mediumHS'];
	}
	if($users[$i]['hardHS'] > $hardHS){
		$hardHS = $users[$i]['hardHS'];
	}
}

$toReturn[0] = $easyHS;
$toReturn[1] = $mediumHS;
$toReturn[2] = $hardHS;

echo json_encode($toReturn);
?>