<?php

$username = $_POST['username'];
$difficulty = $_POST['diff'];
$score = $_POST['points'];

$users = json_decode(file_get_contents("users.json"), true);

function updateUser($users, $username, $difficulty, $score){
	$success = false;
	for($i = 0; $i < sizeof($users); $i++){
		if ($users[$i]['username'] == $username)
		{
			if ($difficulty == 5)
			{
				if ($users[$i]['easyHS'] < $score)
				{
					$users[$i]['easyHS']  = intval($score);
				}
			}
			else if ($difficulty == 10)
			{
				if ($users[$i]['mediumHS'] < $score)
				{
					$users[$i]['mediumHS']  = intval($score);
				}
			}
			else
			{
				if ($users[$i]['hardHS'] < $score)
				{
					$users[$i]['hardHS']  = intval($score);
				}
			}
			file_put_contents("users.json", json_encode($users));
			$success = true;
			break;
		}			
	}
	return $success;
}
echo json_encode(updateUser($users, $username, $difficulty, $score));
?>