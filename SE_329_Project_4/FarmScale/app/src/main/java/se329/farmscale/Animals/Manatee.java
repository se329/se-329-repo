package se329.farmscale.Animals;

import se329.farmscale.R;

/**
 * Created by Alexander on 4/18/2016.
 */
public class Manatee extends Animal{
    @Override
    public String getAnimalDescription() {
        return "The manatee has no natural predators.";
    }

    @Override
    public String getAnimalName(){return "Manatee";}

    @Override
    public int getPicture() {
        return R.drawable.manatee;
    }

    @Override
    public double estimate(String[] userFieldData) {
        return (Double.parseDouble(userFieldData[0])*12) * 25;
    }

    @Override
    public String[] getInputFieldTypes() {
        return new String[]{"ft"};
    }

    @Override
    public String[] getInputFields() {
        return new String[]{"Length"};
    }
}
