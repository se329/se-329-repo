package se329.farmscale.Animals;

import se329.farmscale.R;

/**
 * Created by Alexander on 4/18/2016.
 */
public class Giraffe extends Animal{
    @Override
    public String getAnimalDescription() {
        return "A giraffe's neck is too short to reach the ground.";
    }

    @Override
    public String getAnimalName(){return "Giraffe";}

    @Override
    public int getPicture() {
        return R.drawable.giraffe;
    }

    @Override
    public String[] getInputFieldTypes() {
        return new String[]{"ft", "years"};
    }

    @Override
    public double estimate(String[] userFieldData) {
        return (Double.parseDouble(userFieldData[1])*112);
    }

    @Override
    public String[] getInputFields() {
        return new String[]{"Neck Length", "Age"};
    }
}
