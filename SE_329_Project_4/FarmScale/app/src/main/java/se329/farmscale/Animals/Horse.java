package se329.farmscale.Animals;

import se329.farmscale.R;

/**
 * Created by Eric Swanberg on 4/18/2016.
 */
public class Horse extends Animal {
    @Override
    public String getAnimalDescription() {
        return "The horse is a very common animal on the farm and is used for Horse riding and moving materials. Horses eat hay and are very loyal.";
    }

    @Override
    public String getAnimalName(){return "Horse";}

    @Override
    public int getPicture() {
        return R.drawable.horse;
    }

    @Override
    public String[] getInputFieldTypes() {
        return new String[]{"in", "ft"};
    }

    @Override
    public double estimate(String[] userFieldData) {
        return (Double.parseDouble(userFieldData[0]) * Double.parseDouble(userFieldData[0]) * (Double.parseDouble(userFieldData[1])*12))/330 + 50;
    }

    @Override
    public String[] getInputFields() {
        return new String[]{"Heart Girth","Length"};
    }
}
