package se329.farmscale.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import se329.farmscale.AndroidStorage;
import se329.farmscale.Animals.Horse;
import se329.farmscale.R;

public class Login extends AppCompatActivity {


    ImageView image;
    Button loginButton, createUserButton;
    EditText usernameField, passwordField;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        image = (ImageView) findViewById(R.id.image);

        loginButton = (Button) findViewById(R.id.LoginButton);
        createUserButton = (Button) findViewById(R.id.CreateUserButton);

        usernameField = (EditText) findViewById(R.id.usernameTextField);
        passwordField = (EditText) findViewById(R.id.passwordTextField);

        createUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, SignUp.class));
            }
        });

        //Listens for the loginButton Button to be clicked, then makes call to server to check the login credentials.
        final RequestQueue mRequestQueue;

        // Instantiate the cache
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap

        // Set up the network to use HttpURLConnection as the HTTP client.
        BasicNetwork network = new BasicNetwork(new HurlStack());

        // Instantiate the RequestQueue with the cache and network.
        mRequestQueue = new RequestQueue(cache, network);

        // Start the queue
        mRequestQueue.start();

        loginButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://jenge.ddns.net/329/checkLogin.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                if (response.equals("Failed to find user."))
                                {
                                    Toast.makeText(Login.this,"Invalid Credentials",Toast.LENGTH_LONG).show();
                                }
                                else {
                                    Toast.makeText(Login.this, "Successfully logged in", Toast.LENGTH_LONG).show();

                                    AndroidStorage.user = usernameField.getText().toString();

                                    String[] results = response.split(",");
                                    AndroidStorage.email = results[0];
                                    AndroidStorage.SSN = Integer.parseInt(results[1]);

                                    startActivity(new Intent(Login.this, ChooseAnimal.class));
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(Login.this,"An Error Occurred",Toast.LENGTH_LONG).show();
                            }
                        }){
                    @Override
                    protected Map<String,String> getParams(){
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("Name", usernameField.getText().toString());
                        params.put("Password",passwordField.getText().toString());
                        return params;
                    }
                };
                mRequestQueue.add(stringRequest);
            }
        });
    }
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Login.this, Login.class));
                        moveTaskToBack(true);
                    }
                }).setNegativeButton("No", null).show();
    }

}
