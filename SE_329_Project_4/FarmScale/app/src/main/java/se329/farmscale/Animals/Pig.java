package se329.farmscale.Animals;

import se329.farmscale.R;

/**
 * Created by Alexander on 4/18/2016.
 */
public class Pig extends Animal {
    @Override
    public String getAnimalDescription() {
        return "Pigs are literally fat, messy, bacon growers.";
    }

    @Override
    public String getAnimalName(){return "Pig";}

    @Override
    public int getPicture() {
        return R.drawable.pig;
    }

    @Override
    public double estimate(String[] userFieldData) {
        return (Double.parseDouble(userFieldData[0]) * .0254 * Double.parseDouble(userFieldData[0]) * .0254 * Double.parseDouble(userFieldData[1])*12  * .0254) * 69.3 * 2.2;
    }

    @Override
    public String[] getInputFieldTypes() {
        return new String[]{"in", "ft"};
    }

    @Override
    public String[] getInputFields() {
        return new String[]{"Heart Girth","Length"};
    }
}

