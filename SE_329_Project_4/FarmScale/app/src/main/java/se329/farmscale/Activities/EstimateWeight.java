package se329.farmscale.Activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

import se329.farmscale.AndroidStorage;
import se329.farmscale.R;

public class EstimateWeight extends AppCompatActivity {

    ImageView image;
    TextView animalDescription, animalName;
    TableLayout inputArea;
    Button back, estimate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estimate_weight);

        animalName = (TextView) findViewById(R.id.animalName);
        image = (ImageView) findViewById(R.id.imageViewInEstimateAnimal);
        animalDescription = (TextView) findViewById(R.id.animalDescription);
        inputArea = (TableLayout) findViewById(R.id.inputFieldsTableLayout);
        back = (Button) findViewById(R.id.backButtonEstimateWeight);
        estimate = (Button) findViewById(R.id.estimateWeightButton);

        //Populate animal name
        animalName.setText(AndroidStorage.currentAnimal.getAnimalName());

        //Populate the image
        image.setImageResource(AndroidStorage.currentAnimal.getPicture());

        //Populate the animal description
        animalDescription.setText(AndroidStorage.currentAnimal.getAnimalDescription());

        //Get input types
        final String[] inputTypes = AndroidStorage.currentAnimal.getInputFieldTypes();

        //Get input fields
        final String[] inputFields = AndroidStorage.currentAnimal.getInputFields();

        //Populate the Table
        for(int i = 0; i < inputFields.length; i++)
        {
            TableRow row = new TableRow(this);
            inputArea.addView(row);

            //Put the field name in the row
            TextView fieldName = new TextView(this);
            fieldName.setText(inputFields[i]);
            fieldName.setTextSize(25);
            fieldName.setPadding(0, 0, 50, 0);

            //Put in the text edit input box
            EditText inputField = new EditText(this);
            inputField.setEms(4);
            inputField.setRawInputType(Configuration.KEYBOARD_12KEY);
            inputField.setGravity(Gravity.RIGHT);

            //Put the 'in' after the edit text
            TextView inches = new TextView(this);
            inches.setText(inputTypes[i]);
            inches.setTextSize(18);

            //Add the elements to the row
            row.addView(fieldName);
            row.addView(inputField);
            row.addView(inches);
        }

        //Back button is pressed
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EstimateWeight.this, ChooseAnimal.class));
            }
        });

        final RequestQueue mRequestQueue;

        // Instantiate the cache
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap

        // Set up the network to use HttpURLConnection as the HTTP client.
        BasicNetwork network = new BasicNetwork(new HurlStack());

        // Instantiate the RequestQueue with the cache and network.
        mRequestQueue = new RequestQueue(cache, network);

        // Start the queue
        mRequestQueue.start();

        estimate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] input = new String[AndroidStorage.currentAnimal.getInputFields().length];

                for(int i = 0, j = inputArea.getChildCount(); i < j; i++) {
                    View view = inputArea.getChildAt(i);
                    TableRow row = (TableRow) view;
                    EditText inputBox = (EditText) row.getChildAt(1);
                    input[i] = inputBox.getText().toString();
                }

                final double estimate = AndroidStorage.currentAnimal.estimate(input);   //estimate
                final NumberFormat formatter = new DecimalFormat("#0.00");    //For formatting

                new AlertDialog.Builder(EstimateWeight.this)
                        .setTitle("Estimated Weight")
                        .setMessage("The estimate weight of your " + AndroidStorage.currentAnimal.getAnimalName() + " is: \n" + formatter.format(estimate) + " lbs!")
                        .setIcon(AndroidStorage.currentAnimal.getPicture())
                        .show();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://jenge.ddns.net/329/updateLog.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(EstimateWeight.this,"Updated log",Toast.LENGTH_LONG).show();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(EstimateWeight.this,"Failed to update log",Toast.LENGTH_LONG).show();
                            }
                        }){
                    @Override
                    protected Map<String,String> getParams(){
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("Animal", AndroidStorage.currentAnimal.getAnimalName());
                        params.put("Weight", formatter.format(estimate) +"");
                        params.put("User", AndroidStorage.user);
                        return params;
                    }
                };
                mRequestQueue.add(stringRequest);
            }
        });
    }
}
