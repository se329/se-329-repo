package se329.farmscale.Animals;

import se329.farmscale.R;

/**
 * Created by Alexander on 4/18/2016.
 */
public class Armadillo extends Animal{
    @Override
    public String getAnimalDescription() {
        return "The armadillo has a shell like a bomb shelter. They are mega dope";
    }

    @Override
    public String getAnimalName(){return "Armadillo";}

    @Override
    public int getPicture() {
        return R.drawable.armadillo;
    }

    @Override
    public String[] getInputFieldTypes() {
        return new String[]{"in", "in"};
    }

    @Override
    public double estimate(String[] userFieldData) {
        return (Double.parseDouble(userFieldData[0]) + Double.parseDouble(userFieldData[1])*3);
    }

    @Override
    public String[] getInputFields() {
        return new String[]{"Heart Girth","Length"};
    }
}
