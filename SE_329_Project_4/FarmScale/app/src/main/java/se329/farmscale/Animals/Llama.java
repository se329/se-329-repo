package se329.farmscale.Animals;

import se329.farmscale.R;

/**
 * Created by Alexander on 4/18/2016.
 */
public class Llama extends Animal{
    @Override
    public String getAnimalDescription() {
        return "Tina, you fat lard, come get some DINNER! Eat the FOOD!";
    }

    @Override
    public String getAnimalName(){return "Llama";}

    @Override
    public int getPicture() {
        return R.drawable.llama1;
    }

    @Override
    public double estimate(String[] userFieldData) {
        return (Double.parseDouble(userFieldData[0]) * Double.parseDouble(userFieldData[0]) * (Double.parseDouble(userFieldData[1])*12))/300;
    }

    @Override
    public String[] getInputFieldTypes() {
        return new String[]{"in", "ft"};
    }

    @Override
    public String[] getInputFields() {
        return new String[]{"Heart Girth","Length"};
    }
}
