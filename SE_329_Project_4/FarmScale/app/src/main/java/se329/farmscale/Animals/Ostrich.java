package se329.farmscale.Animals;

import se329.farmscale.R;

import se329.farmscale.R;

/**
 * Created by Alexander on 4/18/2016.
 */
public class Ostrich extends Animal{
    @Override
    public String getAnimalDescription() {
        return "An ostrich can run up to 45 miles per hour.";
    }

    @Override
    public String getAnimalName(){return "Ostrich";}

    @Override
    public int getPicture() {
        return R.drawable.ostrich;
    }

    @Override
    public double estimate(String[] userFieldData) {
        return (Double.parseDouble(userFieldData[0])) * 29;
    }

    @Override
    public String[] getInputFieldTypes() {
        return new String[]{"ft"};
    }

    @Override
    public String[] getInputFields() {
        return new String[]{"Height"};
    }
}
