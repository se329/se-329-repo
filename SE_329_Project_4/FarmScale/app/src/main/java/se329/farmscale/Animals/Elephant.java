package se329.farmscale.Animals;

import se329.farmscale.R;

/**
 * Created by Alexander on 4/18/2016.
 */
public class Elephant extends Animal{
    @Override
    public String getAnimalDescription() {
        return "An elephant never forgets. And they never forgive...";
    }

    @Override
    public String getAnimalName(){return "Elephant";}

    @Override
    public int getPicture() {
        return R.drawable.elephant;
    }

    @Override
    public String[] getInputFieldTypes() {
        return new String[]{"ft", "years"};
    }

    @Override
    public double estimate(String[] userFieldData) {
        return ((Double.parseDouble(userFieldData[0])*12) + .1*Double.parseDouble(userFieldData[1])) * 108.18;
    }

    @Override
    public String[] getInputFields() {
        return new String[]{"Height", "Age"};
    }
}
