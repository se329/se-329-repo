package se329.farmscale.Animals;

import se329.farmscale.R;

/**
 * Created by Alexander on 4/18/2016.
 */
public class Cow extends Animal{
    @Override
    public String getAnimalDescription() {
        return "Cows have four stomachs. I'm not actually sure if that is true.";
    }
    @Override
    public String getAnimalName(){return "Cow";}

    @Override
    public String[] getInputFieldTypes() {
        return new String[]{"in", "ft"};
    }

    @Override
    public int getPicture() {
        return R.drawable.cow;
    }

    @Override
    public double estimate(String[] userFieldData) {
        return (Double.parseDouble(userFieldData[0]) * Double.parseDouble(userFieldData[0]) * (Double.parseDouble(userFieldData[1])*12))/300;
    }

    @Override
    public String[] getInputFields() {
        return new String[]{"Heart Girth","Length"};
    }
}
