package se329.farmscale.Animals;

import se329.farmscale.R;

import se329.farmscale.R;

/**
 * Created by Alexander on 4/18/2016.
 */
public class Chicken extends Animal{
    @Override
    public String getAnimalDescription() {
        return "Bwwaaaaaaaaaaaakkkkk!";
    }

    @Override
    public String getAnimalName(){return "Chicken";}

    @Override
    public int getPicture() {
        return R.drawable.chicken;
    }

    @Override
    public String[] getInputFieldTypes() {
        return new String[]{"in"};
    }

    @Override
    public double estimate(String[] userFieldData) {
        return (Double.parseDouble(userFieldData[0]) / 24 ) + 6;
    }

    @Override
    public String[] getInputFields() {
        return new String[]{"Height"};
    }
}
