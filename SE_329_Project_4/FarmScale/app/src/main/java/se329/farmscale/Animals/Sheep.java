package se329.farmscale.Animals;

import se329.farmscale.R;

/**
 * Created by Alexander on 4/18/2016.
 */
public class Sheep extends Animal {
    @Override
    public String getAnimalDescription() {
        return "Sheep are fluffy and sweet. Most commonly referenced with LSD as you count them before you go to sleep.";
    }

    @Override
    public String getAnimalName(){return "Sheep";}

    @Override
    public int getPicture() {
        return R.drawable.sheep;
    }

    @Override
    public double estimate(String[] userFieldData) {
        return (Double.parseDouble(userFieldData[0])* Double.parseDouble(userFieldData[0])* Double.parseDouble(userFieldData[1])*12)/300;
    }

    @Override
    public String[] getInputFieldTypes() {
        return new String[]{"in", "ft"};
    }

    @Override
    public String[] getInputFields() {

        return new String[]{"Heart Girth","Length"};
    }
}
