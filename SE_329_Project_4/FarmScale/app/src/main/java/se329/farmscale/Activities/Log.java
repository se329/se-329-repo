package se329.farmscale.Activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import se329.farmscale.AndroidStorage;
import se329.farmscale.R;

public class Log extends AppCompatActivity {

    TableLayout table;
    Button backButton;
    TextView logTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

        table = (TableLayout) findViewById(R.id.logTable);
        backButton = (Button) findViewById(R.id.backButtonLog);
        logTitle = (TextView) findViewById(R.id.logTitle);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Log.this, ChooseAnimal.class));
            }
        });

        //Listens for the loginButton Button to be clicked, then makes call to server to check the login credentials.
        final RequestQueue mRequestQueue;

        // Instantiate the cache
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap

        // Set up the network to use HttpURLConnection as the HTTP client.
        BasicNetwork network = new BasicNetwork(new HurlStack());

        // Instantiate the RequestQueue with the cache and network.
        mRequestQueue = new RequestQueue(cache, network);

        // Start the queue
        mRequestQueue.start();

        if(AndroidStorage.currentLog.equals("Global"))
        {
            logTitle.setText("Global Animal Weights");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://jenge.ddns.net/329/getGlobalLog.php",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            String[] results = response.split(",");
                            ArrayList<String[]> mergedResults = new ArrayList<>();

                            int count = 0;
                            String[] merged = new String[3];
                            for (String s : results) {
                                if (count % 3 == 0 && count != 0) {
                                    mergedResults.add(merged);
                                    merged = new String[3];
                                }
                                merged[count % 3] = s.trim();
                                count++;
                            }
                            mergedResults.add(merged);

                            //Add the log data
                            for (String[] log : mergedResults) {
                                TableRow row = new TableRow(Log.this);
                                table.addView(row);

                                //Put the animal name in the row
                                TextView animalName = new TextView(Log.this);
                                animalName.setLayoutParams(new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT,
                                        TableRow.LayoutParams.MATCH_PARENT,
                                        1.0f
                                ));
                                animalName.setText(log[0]);
                                animalName.setTextSize(25);

                                //Put the animal count in the row
                                TextView animalCount = new TextView(Log.this);
                                animalCount.setLayoutParams(new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT,
                                        TableRow.LayoutParams.MATCH_PARENT,
                                        1.0f
                                ));
                                animalCount.setText(log[1]);
                                animalCount.setTextSize(25);

                                //Put the animal weight in the row
                                TextView animalWeight = new TextView(Log.this);
                                animalWeight.setLayoutParams(new TableRow.LayoutParams(
                                        TableRow.LayoutParams.MATCH_PARENT,
                                        TableRow.LayoutParams.MATCH_PARENT,
                                        1.0f
                                ));
                                animalWeight.setText(log[2]);
                                animalWeight.setTextSize(25);

                                row.addView(animalName);
                                row.addView(animalCount);
                                row.addView(animalWeight);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(Log.this, "Could not retrieve animal logs.", Toast.LENGTH_LONG).show();
                        }
                    }) {
            };
            mRequestQueue.add(stringRequest);
        }

        else{
            logTitle.setText("Your Animal Weights");
        StringRequest stringRequest2 = new StringRequest(Request.Method.POST, "http://jenge.ddns.net/329/getUserLog.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String[] results = response.split(",");
                        ArrayList<String[]> mergedResults = new ArrayList<>();

                        int count = 0;
                        String[] merged = new String[3];
                        for (String s : results)
                        {
                            if (count % 3 == 0 && count != 0) {
                                mergedResults.add(merged);
                                merged = new String[3];
                            }
                            merged[count % 3] = s.trim();
                            count++;
                        }
                        mergedResults.add(merged);

                        //Add the log data
                        for(String[] log : mergedResults)
                        {
                            TableRow row = new TableRow(Log.this);
                            table.addView(row);

                            //Put the animal name in the row
                            TextView animalName = new TextView(Log.this);
                            animalName.setLayoutParams(new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT,
                                    TableRow.LayoutParams.MATCH_PARENT,
                                    1.0f
                            ));
                            animalName.setText(log[0]);
                            animalName.setTextSize(25);

                            //Put the animal count in the row
                            TextView animalCount = new TextView(Log.this);
                            animalCount.setLayoutParams(new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT,
                                    TableRow.LayoutParams.MATCH_PARENT,
                                    1.0f
                            ));
                            animalCount.setText(log[1]);
                            animalCount.setTextSize(25);

                            //Put the animal weight in the row
                            TextView animalWeight = new TextView(Log.this);
                            animalWeight.setLayoutParams(new TableRow.LayoutParams(
                                    TableRow.LayoutParams.MATCH_PARENT,
                                    TableRow.LayoutParams.MATCH_PARENT,
                                    1.0f
                            ));
                            animalWeight.setText(log[2]);
                            animalWeight.setTextSize(25);

                            row.addView(animalName);
                            row.addView(animalCount);
                            row.addView(animalWeight);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Log.this,"Could not retrieve animal logs.",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("User", AndroidStorage.user);
                return params;
            }
        };
        mRequestQueue.add(stringRequest2);
        }
    }
}
