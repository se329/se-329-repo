package se329.farmscale.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import se329.farmscale.AndroidStorage;
import se329.farmscale.Animals.Armadillo;
import se329.farmscale.Animals.Chicken;
import se329.farmscale.Animals.Cow;
import se329.farmscale.Animals.Elephant;
import se329.farmscale.Animals.Giraffe;
import se329.farmscale.Animals.Horse;
import se329.farmscale.Animals.Llama;
import se329.farmscale.Animals.Manatee;
import se329.farmscale.Animals.Ostrich;
import se329.farmscale.Animals.Pig;
import se329.farmscale.Animals.Sheep;
import se329.farmscale.R;

public class ChooseAnimal extends AppCompatActivity {

    ImageView horse, cow, pig, sheep, armadillo, llama, chicken, elephant, giraffe, manatee, ostrich;
    Button logButton, userLogButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_animal);

        horse = (ImageView) findViewById(R.id.Horse);
        cow = (ImageView) findViewById(R.id.Cow);
        pig = (ImageView) findViewById(R.id.Pig);
        sheep = (ImageView) findViewById(R.id.Sheep);
        armadillo = (ImageView) findViewById(R.id.Armadillo);
        llama = (ImageView) findViewById(R.id.Llama);
        chicken = (ImageView) findViewById(R.id.Chicken);
        elephant = (ImageView) findViewById(R.id.Elephant);
        manatee = (ImageView) findViewById(R.id.Manatee);
        ostrich = (ImageView) findViewById(R.id.Ostrich);
        giraffe = (ImageView) findViewById(R.id.Giraffe);

        logButton = (Button) findViewById(R.id.logButton);
        userLogButton = (Button) findViewById(R.id.userLogButton);

        logButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidStorage.currentLog = "Global";
                startActivity(new Intent(ChooseAnimal.this, Log.class));}
        });

        userLogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidStorage.currentLog = "User";
                startActivity(new Intent(ChooseAnimal.this, Log.class));}
        });

        horse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidStorage.currentAnimal = new Horse();
                startActivity(new Intent(ChooseAnimal.this, EstimateWeight.class));
            }
        });

        cow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidStorage.currentAnimal = new Cow();
                startActivity(new Intent(ChooseAnimal.this, EstimateWeight.class));
            }
        });

        pig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidStorage.currentAnimal = new Pig();
                startActivity(new Intent(ChooseAnimal.this, EstimateWeight.class));
            }
        });

        sheep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidStorage.currentAnimal = new Sheep();
                startActivity(new Intent(ChooseAnimal.this, EstimateWeight.class));
            }
        });

        chicken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidStorage.currentAnimal = new Chicken();
                startActivity(new Intent(ChooseAnimal.this, EstimateWeight.class));
            }
        });

        elephant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidStorage.currentAnimal = new Elephant();
                startActivity(new Intent(ChooseAnimal.this, EstimateWeight.class));
            }
        });

        giraffe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidStorage.currentAnimal = new Giraffe();
                startActivity(new Intent(ChooseAnimal.this, EstimateWeight.class));
            }
        });

        manatee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidStorage.currentAnimal = new Manatee();
                startActivity(new Intent(ChooseAnimal.this, EstimateWeight.class));
            }
        });

        ostrich.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidStorage.currentAnimal = new Ostrich();
                startActivity(new Intent(ChooseAnimal.this, EstimateWeight.class));
            }
        });

        armadillo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidStorage.currentAnimal = new Armadillo();
                startActivity(new Intent(ChooseAnimal.this, EstimateWeight.class));
            }
        });

        llama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidStorage.currentAnimal = new Llama();
                startActivity(new Intent(ChooseAnimal.this, EstimateWeight.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(ChooseAnimal.this)
                .setTitle("Logout")
                .setMessage("Are you sure you want to logout")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(ChooseAnimal.this, Login.class));
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
