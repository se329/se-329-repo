package se329.farmscale.Animals;

import android.widget.ImageView;
import android.widget.TableLayout;

/**
 * Created by Eric Swanberg on 4/18/2016.
 */
public abstract class Animal {
    public abstract String getAnimalName();
    public abstract String getAnimalDescription();
    public abstract int getPicture();
    public abstract String[] getInputFields();
    public abstract String[] getInputFieldTypes();
    public abstract double estimate(String[] userFieldData);
}
