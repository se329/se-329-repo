package project1;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import org.jdom2.JDOMException;

import net.coobird.thumbnailator.Thumbnails;
import project1.pojo.Attendance.Student;
import project1.pojo.FaceMap;
import project1.pojo.Node;

public class RunBetaFace extends GUI
{
	public RunBetaFace() throws IOException, JDOMException, InterruptedException {
		super();
	}

	/**
	 * Get the picture, get faces from it, and compare to current students in database
	 * @param filePath
	 * @param className
	 * @return
	 * @throws IOException
	 * @throws JDOMException
	 * @throws InterruptedException
	 */
	public static ArrayList<Student> run(String filePath, String className) throws IOException, JDOMException, InterruptedException
	{
		//Get time
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String timeTaken = dateFormat.format(date);

		//Get faces from image
		Image img = new Image(filePath); 
		ArrayList<Face> faces = img.getFaces(); 

		
		
		//This facemap currently is not being used anywhere, we simply just construct the facemap
		//Make the facemap
		FaceMap faceMap = new FaceMap();

		//Dont need anymore?
		//Construct nodes for each face
		for(Face f : faces)
		{
			faceMap.addNode(new Node(timeTaken, f));
		}
		//This has not been tested at all
		faceMap.createFaceMap();

		
		
		
		ArrayList<Student> studentsInPicture = findStudentsInPhoto(faces, className);

		putNamesOnPicture(faces, studentsInPicture, filePath);

		return studentsInPicture;
	}


	/**
	 * put students names on the picture
	 * @param faces
	 * @param studentsInPicture
	 * @param filePath
	 */
	private static void putNamesOnPicture(ArrayList<Face> faces, ArrayList<Student> studentsInPicture, String filePath) 
	{
		try
		{
			BufferedImage image = ImageIO.read(new File(filePath));

			for(Student student : studentsInPicture)
			{
				Person person = new Person(); 
				person.setName(student.getName());
				for(String uid : student.getUids())
					person.addUID(uid); 

				double x = 0.0, y = 0.0;

				for(Face face : faces)
				{
					float confidence = person.compareWithUIDsForConfidence(new ArrayList<String>(Arrays.asList(face.getUID())));
					if(confidence == -1)
						return;
					
					if(confidence >= 0.7)
					{
						x = face.getX();
						y = face.getY();
						attendance.updateFaceDescription(face, student.getClassName(), student.getName());
						break;
					}
				}
				if(x != 0.0 && y != 0.0)
				{
					String name = student.getName();
					String[] nameParsed = name.split(" ");

					Graphics g = image.getGraphics();
					g.setFont(g.getFont().deriveFont(50f));
					g.drawString(nameParsed[0], (int)x - 20, (int)y);
					if(nameParsed.length > 1)
						g.drawString(nameParsed[1], (int)x - 20, (int)y + 40);
					g.dispose();
				}
			}
			String fileName = filePath.substring(0, filePath.indexOf(".")) + "_FaceRecognition.png";

			image = Thumbnails.of(image).size(1000, 1000).asBufferedImage();

			ImageIO.write(image, "png", new File(fileName));

		
			JLabel lbl = new JLabel(new ImageIcon(fileName));
			JOptionPane.showMessageDialog(contentPane, lbl, "ImageDialog", 
					JOptionPane.PLAIN_MESSAGE, null);
		}
		catch(Exception e)
		{

		}
	}


	/**
	 * See what students are in the picture
	 * @param faces
	 * @param className
	 * @return
	 * @throws IOException
	 * @throws JDOMException
	 * @throws InterruptedException
	 */
	private static ArrayList<Student> findStudentsInPhoto(ArrayList<Face> faces, String className) throws IOException, JDOMException, InterruptedException 
	{
		//Face ids in picture
		ArrayList<String> facesIdsPicture = new ArrayList<String>(); 
		for(Face f : faces)
			facesIdsPicture.add(f.getUID()); 

		ArrayList<Student> studentsInPicture = new ArrayList<Student>(); 

		//Compare faces in photo to the attendance database
		for(Student student : attendance.getStudentList(className))
		{
			Person person = new Person(); 
			person.setName(student.getName());
			for(String uid : student.getUids())
				person.addUID(uid); 

			if(person.compareWithUIDsForConfidence(facesIdsPicture) >= 0.5)
			{
				studentsInPicture.add(student);
				student.addToAttendance();
			}
		};
		return studentsInPicture;
	}
}