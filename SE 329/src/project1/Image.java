package project1;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import project1.pojo.TagInfo; 


public class Image extends BetaFace{ 

	private byte[] imageBytes; 
	private static Logger log = Logger.getLogger(Image.class.getName()); 

	public Image(String file) throws IOException { 

		if(debug) 
			log.info("Build image from file"); 

		ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
		copy(new FileInputStream(file), baos, 1024); 
		this.imageBytes = baos.toByteArray(); 
	}     

	public Image(InputStream is) throws IOException { 

		if(debug) 
			log.info("Build image from InputStream"); 

		ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
		copy(is, baos, 1024); 
		this.imageBytes = baos.toByteArray(); 
	} 


	public static void copy(InputStream input, 
			OutputStream output, 
			int bufferSize) 
					throws IOException { 
		byte[] buf = new byte[bufferSize]; 
		int bytesRead = input.read(buf); 
		while (bytesRead != -1) { 
			output.write(buf, 0, bytesRead); 
			bytesRead = input.read(buf); 
		} 
		output.flush(); 
	} 


	public ArrayList<Face> getFaces () throws IOException, JDOMException, InterruptedException { 

		if(debug) 
			log.info("Retriving faces from image"); 

		Element rootElement = new Element("ImageRequestBinary"); 

		rootElement.addContent(new Element("api_key").addContent(apiKey)); 
		rootElement.addContent(new Element("api_secret").addContent(apiSecret)); 
		rootElement.addContent(new Element("detection_flags").addContent("0")); 
		rootElement.addContent(new Element("imagefile_data").addContent(encode64(imageBytes))); 
		rootElement.addContent(new Element("original_filename").addContent("temp.jpg")); 

		String content = process("/UploadNewImage_File", new Document(rootElement)); 

		SAXBuilder builder = new SAXBuilder(); 
		Reader in = new StringReader(content); 

		Document resp = builder.build(in); 
		Element root = resp.getRootElement(); 
		String uid = root.getChild("img_uid").getText(); 

		if(debug) 
			log.info("Image UID = "+uid); 

		ArrayList<Face> faceList = new ArrayList<Face>(); 

		int count = 0;
		while(true) 
		{ 
			if(count > 20)
				return null;
			
			faceList = getUid(uid); 
			if(faceList.size() != 0) 
			{ 
				return faceList; 
			} 
			else 
			{ 
				count++;
				if(debug) 
					log.info("Waiting for response"); 
				Thread.sleep(1000); 
			} 

		} 
	} 

	private ArrayList<Face> getUid(String uid) throws IOException, JDOMException { 

		ArrayList<Face> faces = new ArrayList<Face>(); 

		Element rootElement = new Element("ImageInfoRequestUid"); 

		rootElement.addContent(new Element("api_key").addContent(apiKey)); 
		rootElement.addContent(new Element("api_secret").addContent(apiSecret)); 
		rootElement.addContent(new Element("img_uid").addContent(uid)); 

		String contents = process("/GetImageInfo", new Document(rootElement), false); 

		log.info("Contents: " + contents); 

		contents = contents.trim().replaceFirst("^([\\W]+)<","<"); 
		SAXBuilder builder = new SAXBuilder(); 
		Reader in = new StringReader(contents); 

		Document resp = builder.build(in); 
		Element root = resp.getRootElement(); 
		String uid2 = root.getChild("int_response").getText(); 

		if(Integer.valueOf(uid2) == 1) 
		{ 
			return faces; 
		} 

		List<Element> facesRoot = root.getChild("faces").getChildren("FaceInfo"); 

		for(int i = 0; i < facesRoot.size(); i++) 
		{ 
			Element faceInfo = facesRoot.get(i); 
			Face face = new Face(); 
			
			double x = Double.parseDouble(faceInfo.getChild("x").getText());
			double y = Double.parseDouble(faceInfo.getChild("y").getText());
			
			face.setX(x);
			face.setY(y);
			
			List<Element> information = faceInfo.getChild("tags").getChildren("TagInfo");
			for(int j = 0; j < information.size(); j++)
			{
				Element tagInfo = information.get(j); 
				
				String name = tagInfo.getChild("name").getText();
				String value =  tagInfo.getChild("value").getText();
				double confidence = Double.parseDouble(tagInfo.getChild("confidence").getText());
				
				face.addInformation(new TagInfo(name, value, confidence));
			}

			face.setUID(faceInfo.getChild("uid").getText()); 

			faces.add(face); 
			if(debug) 
				log.info("Process face "+i+", UID = "+faceInfo.getChild("uid").getText()); 
		} 

		return faces; 
	} 


	public String encode64(byte[] image) 
	{ 
		byte[] encoded = Base64.encodeBase64(image); 
		return new String(encoded); 
	} 


	public byte[] decode64(String image) 
	{ 
		return new Base64().decode(image); 
	} 

	public byte[] getImageBytes() { 
		return imageBytes; 
	} 
}
