package project1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdom2.JDOMException;

import com.google.gson.Gson;

import project1.pojo.Attendance;
import project1.pojo.Attendance.Student;

public class GUI extends JFrame {

	private static final long serialVersionUID = 1L;

	protected static JPanel contentPane;

	private JTextField classPhotoFilePath;

	public JList selectedStudentName;
	private JTextField studentPictureFilePath;
	private JTextField studentNameTextField;
	private JTextArea studentInfoField;

	protected static Attendance attendance;

	private JComboBox classComboBox;

	private DataModel dataModel;

	private JLabel attendanceValue;

	protected static final String attendanceJsonFilePath = "Attendance.json";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws InterruptedException 
	 * @throws JDOMException 
	 * @throws IOException 
	 */
	public GUI() throws IOException, JDOMException, InterruptedException 
	{
		setDefaultValues();

		createComboBoxes();

		createButtons();

		createTextFields();

		createJLabels();

		createListTables();

		createBorderPanels();
	}


	private void createComboBoxes() 
	{
		classComboBox = new JComboBox(attendance.getClasses());
		classComboBox.setBounds(39, 78, 112, 20);
		classComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(classComboBox.getSelectedItem() != null)
					dataModel.setValues(attendance.getStudentNames(classComboBox.getSelectedItem().toString()));
				else
					dataModel.setValues(new ArrayList<String>());
				studentInfoField.setText("");
			}
		});
		contentPane.add(classComboBox);
	}

	private void createJLabels()
	{
		JLabel lblAttendance = new JLabel("Attendance: ");
		lblAttendance.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblAttendance.setBounds(352, 258, 109, 14);
		contentPane.add(lblAttendance);

		attendanceValue = new JLabel("");
		attendanceValue.setBounds(466, 258, 63, 14);
		contentPane.add(attendanceValue);

		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setBounds(352, 283, 46, 14);
		contentPane.add(lblNewLabel);

		JLabel lblValue = new JLabel("Value");
		lblValue.setBounds(431, 283, 46, 14);
		contentPane.add(lblValue);

		JLabel lblConfidence = new JLabel("Confidence");
		lblConfidence.setBounds(502, 283, 69, 14);
		contentPane.add(lblConfidence);

		JLabel lblInsertStudentName = new JLabel("Insert Student Name");
		lblInsertStudentName.setBounds(28, 369, 143, 14);
		contentPane.add(lblInsertStudentName);

		JLabel lblChooseStudentPicture = new JLabel("Choose Student Picture to Add new Student");
		lblChooseStudentPicture.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblChooseStudentPicture.setBounds(28, 290, 274, 14);
		contentPane.add(lblChooseStudentPicture);

		JLabel lblFacialRecognition = new JLabel("Facial Recognition");
		lblFacialRecognition.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblFacialRecognition.setBounds(179, 0, 242, 47);
		contentPane.add(lblFacialRecognition);

		JLabel lblChoosePicture = new JLabel("Choose Picture for class Attendance");
		lblChoosePicture.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblChoosePicture.setBounds(28, 145, 274, 14);
		contentPane.add(lblChoosePicture);

		JLabel lblStudents = new JLabel("Students");
		lblStudents.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblStudents.setBounds(352, 66, 94, 14);
		contentPane.add(lblStudents);

		JLabel lblChooseAClass = new JLabel("Choose a class");
		lblChooseAClass.setBounds(39, 56, 107, 14);
		contentPane.add(lblChooseAClass);
	}


	private void createListTables() throws FileNotFoundException
	{
		JPanel panel_3 = new JPanel(new BorderLayout());
		panel_3.setBounds(352, 86, 219, 118);
		contentPane.add(panel_3);

		if(classComboBox.getItemCount() == 0)
			dataModel = new DataModel();
		else
			dataModel = new DataModel(attendance.getStudentNames(classComboBox.getSelectedItem().toString()));

		JList<String> listOfStudents = new JList<String>(dataModel);
		listOfStudents.setCellRenderer(new SelectRenderer());
		listOfStudents.addListSelectionListener(
				new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent e) {
						if (!e.getValueIsAdjusting()) {
							selectedStudentName = (JList) e.getSource();
						}
					}	
				}				
				);

		JScrollPane scrollPane = new JScrollPane(listOfStudents);
		panel_3.add(scrollPane, BorderLayout.CENTER);
	}

	private void createBorderPanels() 
	{
		JPanel attendanceBorderPanel = new JPanel();
		attendanceBorderPanel.setBorder(BorderFactory.createTitledBorder("Take Attendance"));
		attendanceBorderPanel.setBounds(10, 125, 318, 141);
		contentPane.add(attendanceBorderPanel);

		JPanel studentInfoBorderPanel = new JPanel();
		studentInfoBorderPanel.setBorder(BorderFactory.createTitledBorder("Get Student Info"));
		studentInfoBorderPanel.setBounds(338, 43, 254, 417);
		contentPane.add(studentInfoBorderPanel);

		JPanel newStudentBorderPanel = new JPanel();
		newStudentBorderPanel.setBorder(BorderFactory.createTitledBorder("Upload New Student"));
		newStudentBorderPanel.setBounds(10, 272, 318, 185);
		contentPane.add(newStudentBorderPanel);
	}

	private void createTextFields() 
	{
		studentPictureFilePath = new JTextField();
		studentPictureFilePath.setColumns(10);
		studentPictureFilePath.setBounds(28, 309, 274, 20);
		contentPane.add(studentPictureFilePath);

		studentNameTextField = new JTextField();
		studentNameTextField.setBounds(28, 388, 170, 20);
		contentPane.add(studentNameTextField);
		studentNameTextField.setColumns(10);

		classPhotoFilePath = new JTextField();
		classPhotoFilePath.setBounds(28, 164, 274, 20);
		contentPane.add(classPhotoFilePath);
		classPhotoFilePath.setColumns(10);

		studentInfoField = new JTextArea();
		studentInfoField.setBounds(352, 308, 219, 134);
		contentPane.add(studentInfoField);
		studentInfoField.setColumns(10);
		Border border = BorderFactory.createLineBorder(Color.BLACK);
		studentInfoField.setBorder(BorderFactory.createCompoundBorder(border, 
				BorderFactory.createEmptyBorder(10, 10, 10, 10)));
	}

	private void createButtons()
	{
		JButton btnDeleteClass = new JButton("Delete Class");
		btnDeleteClass.setBounds(166, 94, 109, 23);
		btnDeleteClass.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				attendance.deleteAllInClass(classComboBox.getSelectedItem().toString());
				classComboBox.removeItem(classComboBox.getSelectedItem().toString());
				try 
				{
					updateFile();
				} 
				catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
			}
		});
		contentPane.add(btnDeleteClass);

		JButton btnAddNewClass = new JButton("Add New Class");
		btnAddNewClass.setBounds(166, 63, 133, 23);
		btnAddNewClass.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				String className = (String)JOptionPane.showInputDialog(
						contentPane,
						"Enter name for your new class",
						"New Class Name",
						JOptionPane.PLAIN_MESSAGE,
						null,
						null,
						null);
				if(className.length() != 0)
				{
					if(!attendance.doesClassExists(className))
					{
						classComboBox.addItem(className);
						classComboBox.setSelectedItem(className);
					}
					else
						JOptionPane.showMessageDialog(new JFrame(), "Class already exists!", "Failed",
								JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		contentPane.add(btnAddNewClass);

		JButton uploadNewStudentButton = new JButton("Upload New Student");
		uploadNewStudentButton.setBounds(28, 419, 170, 23);
		uploadNewStudentButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				try 
				{
					boolean success = attendance.addNewStudent(studentNameTextField.getText(), studentPictureFilePath.getText(), classComboBox.getSelectedItem().toString());

					if(!success)
						JOptionPane.showMessageDialog(new JFrame(), "Could not recognize face!", "Failed",
								JOptionPane.ERROR_MESSAGE);
					else
					{
						updateFile();
						dataModel.addElement(studentNameTextField.getText());
					}
				}
				catch (IOException e1) {
					e1.printStackTrace();
				} catch (JDOMException e1) {
					e1.printStackTrace();
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		});
		contentPane.add(uploadNewStudentButton);

		JButton browseForStudentPhoto = new JButton("Browse");
		browseForStudentPhoto.setBounds(28, 335, 89, 23);
		browseForStudentPhoto.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				final JFileChooser fc = new JFileChooser();
				int returnVal = fc.showOpenDialog(GUI.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) 
				{
					File file = fc.getSelectedFile();
					studentPictureFilePath.setText(file.getAbsolutePath());
				}
			}
		});
		contentPane.add(browseForStudentPhoto);

		JButton getInfoButton = new JButton("Get Student Info");
		getInfoButton.setBounds(442, 215, 129, 23);
		getInfoButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(selectedStudentName != null)
				{
					studentInfoField.setText(attendance.getStudentInfo(classComboBox.getSelectedItem().toString(), selectedStudentName.getSelectedValue().toString()));
					attendanceValue.setText(attendance.getStudentAttendance(classComboBox.getSelectedItem().toString(), selectedStudentName.getSelectedValue().toString()));
				}
			}
		});
		contentPane.add(getInfoButton);

		JButton browseButton = new JButton("Browse");
		browseButton.setBounds(28, 195, 89, 23);
		browseButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				final JFileChooser fc = new JFileChooser();
				int returnVal = fc.showOpenDialog(GUI.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) 
				{
					File file = fc.getSelectedFile();
					classPhotoFilePath.setText(file.getAbsolutePath());
				}
			}
		});
		contentPane.add(browseButton);

		JButton uploadButton = new JButton("Upload for Attendance");
		uploadButton.setBounds(28, 229, 170, 23);
		uploadButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				try 
				{
					ArrayList<Student> studentsInPicture = RunBetaFace.run(classPhotoFilePath.getText(), classComboBox.getSelectedItem().toString());

					String names = "<html><body width='200'<h1>Students In Photo</h1><p>";
					for(Student s : studentsInPicture)
						names += s.getName() + " Attendance: " + s.getAttendance() + "<br>";
					JOptionPane.showMessageDialog(contentPane, names);
					updateFile();
				} 
				catch (IOException | JDOMException | InterruptedException e1) 
				{
					e1.printStackTrace();
				}
			}
		});
		contentPane.add(uploadButton);

		JButton deleteButton = new JButton("Delete");
		deleteButton.setBounds(352, 215, 89, 23);
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(selectedStudentName != null)
				{
					try 
					{
						attendance.deleteStudent(selectedStudentName.getSelectedValue().toString(), classComboBox.getSelectedItem().toString());
						dataModel.removeElement();
						updateFile();
					} 
					catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		contentPane.add(deleteButton);
	}

	private void setDefaultValues() 
	{
		setResizable(false);
		setTitle("Facial Recognition");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 618, 503);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		attendance = new Attendance();

		Gson gson = new Gson();

		BufferedReader br = null;
		try 
		{
			br = new BufferedReader(new FileReader(attendanceJsonFilePath));
			attendance = gson.fromJson(br, Attendance.class);
		} 
		catch (Exception e) 
		{
			attendance = new Attendance();
		}
	}


	public class DataModel extends AbstractListModel<String> {
		private static final long serialVersionUID = 1L;

		public ArrayList<String> dataList;

		public DataModel(ArrayList<String> names) throws FileNotFoundException 
		{
			dataList = names;
		}

		public DataModel() {
			dataList = new ArrayList<>();
		}

		public void setValues(ArrayList<String> studentNames) 
		{
			dataList = studentNames;
			fireContentsChanged(this, 0, dataList.size() - 1);
		}

		@Override
		public int getSize() {
			return dataList.size();
		}

		@Override
		public String getElementAt(int index) {
			return dataList.get(index);
		}

		public void addElement (String s) {
			if(!dataList.contains(s))
			{
				dataList.add(s);		
				fireIntervalAdded(this,dataList.size()-1, dataList.size()-1);
			}
		}

		public void removeElement () {
			dataList.remove(selectedStudentName.getSelectedValue().toString());
			this.fireIntervalRemoved(this, dataList.size()-1, dataList.size()-1);
		}

		public void update() {
			fireContentsChanged(this, 0, dataList.size() - 1);
		}
	}


	class SelectRenderer implements ListCellRenderer <Object>
	{
		@Override
		public Component getListCellRendererComponent(JList <?> list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {

			JLabel label = new JLabel((String)value);
			label.setOpaque(true);

			if (isSelected)
			{
				label.setBackground(Color.yellow);
			}
			else 
			{
				label.setBackground(Color.white);
			}
			return label;
		}
	}


	private void updateFile() throws FileNotFoundException 
	{
		Gson gson = new Gson();
		String json = gson.toJson(attendance);

		try 
		{
			FileWriter writer = new FileWriter(attendanceJsonFilePath);
			writer.write(json);
			writer.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
