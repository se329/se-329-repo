package project1;

import java.util.ArrayList;

import project1.pojo.TagInfo;

public class Face extends BetaFace { 
	 
    private String faceUID; 
    private ArrayList<TagInfo> information;
    private double x, y;
    
    public double getX() 
    {
		return x;
	}

	public void setX(double x)
	{
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public Face()
    {
    	information = new ArrayList<TagInfo>();
    }
    
    public void addInformation(TagInfo tagInfo)
    {
    	information.add(tagInfo);
    }
    
    public ArrayList<TagInfo> getInformation()
    {
		return information;
	}

	public void setInformation(ArrayList<TagInfo> information) 
	{
		this.information = information;
	}

	public void setUID (String uid) 
    { 
        faceUID = uid; 
    } 
 
    public String getUID () 
    { 
        return faceUID; 
    } 
}