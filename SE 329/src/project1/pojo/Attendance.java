package project1.pojo;

import java.io.IOException;
import java.util.ArrayList;

import org.jdom2.JDOMException;

import project1.Face;
import project1.Image;
import project1.Person;

public class Attendance
{
	protected static final String attendanceJsonFilePath = "Attendance.json";

	private ArrayList<Student> students;

	public Attendance()
	{
		students = new ArrayList<Student>();
	}

	/**
	 * Update the student with a new face description
	 * @param face
	 * @param className
	 * @param name
	 */
	public void updateFaceDescription(Face face, String className, String name)
	{
		for(Student s : students)
			if(s.getClassName().equals(className) && s.getName().equals(name))
				s.setFaceInformation(face.getInformation());
	}

	/**
	 * Delete all students in the class
	 * @param className
	 */
	public void deleteAllInClass(String className) 
	{
		ArrayList<String> studentNames = getStudentNames(className);
		for(String name : studentNames)
			deleteStudent(name, className);
	}

	/**
	 * Delete a specific student from a class
	 * @param name
	 * @param className
	 */
	public void deleteStudent(String name, String className)
	{
		for(int i = 0 ; i < students.size(); i++)
			if(students.get(i).getName().equals(name) && students.get(i).getClassName().equals(className))
			{
				students.remove(i);
				break;
			}
	}

	/**
	 * Find all student from a class
	 * @param className
	 * @return
	 */
	public ArrayList<Student> getStudentList(String className)
	{
		ArrayList<Student> studentsInClass = new ArrayList<Student>();
		for(Student s : students)
			if(s.getClassName().equals(className))
				studentsInClass.add(s);
		return studentsInClass;
	}

	/**
	 * Find all all the sudent names from a class
	 * @param className
	 * @return
	 */
	public ArrayList<String> getStudentNames(String className)
	{
		ArrayList<String> studentNames = new ArrayList<String>();
		for(Student s : students)
			if(s.getClassName().equals(className))
				studentNames.add(s.getName());
		return studentNames;
	}

	/**
	 * Add a new student to the database, or update a current student with another picture
	 * @param studentName
	 * @param filePath
	 * @param className
	 * @throws IOException
	 * @throws JDOMException
	 * @throws InterruptedException
	 */
	public boolean addNewStudent(String studentName, String filePath, String className) throws IOException, JDOMException, InterruptedException
	{
		Image img = new Image(filePath); 
		ArrayList<Face> face = img.getFaces(); 

		//Failed to get the main face
		//Throw an error?
		if(face == null)
			return false;
		
		Person eric = new Person(); 
		eric.addUID(face.get(0).getUID()); 
		eric.setName(studentName); 
		eric.rememberPerson(); 

		if(getStudentNames(className).contains(studentName))
		{
			for(Student s : students)
				if(s.getName().equals(studentName))
					s.addUid(face.get(0).getUID());
		}
		else
			students.add(new Student(studentName, className, face.get(0).getInformation()));
		
		return true;
	}

	/**
	 * Constructs a list of all face values from a student
	 * @param className
	 * @param studentName
	 * @return
	 */
	public String getStudentInfo(String className, String studentName)
	{
		Student selectedStudent = null;
		for(Student s : students)
			if(s.getClassName().equals(className) && s.getName().equals(studentName))
				selectedStudent = s;

		String info = "";
		for(TagInfo t : selectedStudent.getInformation())
			info += t.getName() + ":\t" + t.getValue() + "\t" + t.getConfidence() + "\n";

		return info;
	}

	/**
	 * Get the students current attendance
	 * @param className
	 * @param name
	 * @return
	 */
	public String getStudentAttendance(String className, String name)
	{
		for(Student s : students)
			if(s.getName().equals(name) && s.getClassName().equals(className))
				return s.getAttendance() + "";
		return "";
	}

	/**
	 * Check if the class exists
	 * @param className
	 * @return
	 */
	public boolean doesClassExists(String className)
	{
		Object[] classesInDatabase = getClasses();
		for(Object classInDatabase : classesInDatabase)
			if(classInDatabase.toString().equals(className))
				return true;
		return false;
	}
	
	/**
	 * Find all classes from the database
	 * @return
	 */
	public Object[] getClasses()
	{
		ArrayList<String> classes = new ArrayList<String>();
		for(Student s : students)
			if(!classes.contains(s.getClassName()))
				classes.add(s.getClassName());
		return classes.toArray();
	}

	public class Student
	{
		private String name, className;
		private int count; 
		private ArrayList<String> uids;
		private ArrayList<TagInfo> faceInformation;

		public Student(String name, String className, ArrayList<TagInfo> faceInformation)
		{
			this.name = name;
			this.className = className;
			this.faceInformation = faceInformation;
			this.uids = new ArrayList<String>();
		}

		public void setFaceInformation(ArrayList<TagInfo> faceInformation)
		{
			this.faceInformation = faceInformation;
		}

		public ArrayList<TagInfo> getInformation()
		{
			return faceInformation;
		}

		public ArrayList<String> getUids()
		{
			return uids;
		}

		public void addUid(String uid)
		{
			uids.add(uid);
		}

		public void addToAttendance()
		{
			this.count++;
		}

		public String getName() 
		{
			return name;
		}

		public void setName(String name) 
		{
			this.name = name;
		}

		public String getClassName() 
		{
			return className;
		}

		public void setClassName(String className) 
		{
			this.className = className;
		}

		public int getAttendance() 
		{
			return count;
		}

		public void setCount(int count) 
		{
			this.count = count;
		}
	}
}
