package project1.pojo;

import java.util.ArrayList;

import project1.Face;

public class Node 
{
	private String timeTaken;
	private String imageUID;
	private ArrayList<TagInfo> information;
	private double x, y;

	public Node(String timeTaken, Face face)
	{
		this.x = face.getX();
		this.y = face.getY();
		this.imageUID = face.getUID();
		this.information = face.getInformation();
		this.timeTaken = timeTaken;
	}
	public Node(){
		
	}
	public String getTimeTaken() 
	{
		return timeTaken;
	}

	public double getX() 
	{
		return x;
	}

	public double getY() 
	{
		return y;
	}

	public String getImageUID() 
	{
		return imageUID;
	}

	public ArrayList<TagInfo> getInformation() 
	{
		return information;
	}



}
