package project1.pojo;

public class TagInfo 
{
	private String name, value;
	private double confidence;
	
	public TagInfo(String name, String value, double confidence)
	{
		this.name = name;
		this.value = value;
		this.confidence = confidence;
	}
	
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public String getValue() 
	{
		return value;
	}
	
	public void setValue(String value)
	{
		this.value = value;
	}
	
	public double getConfidence()
	{
		return confidence;
	}
	
	public void setConfidence(double confidence) 
	{
		this.confidence = confidence;
	}
}
