package project1.pojo;

import java.util.ArrayList;

public class FaceMap 
{
	private ArrayList<Edge> edgesList = new ArrayList<Edge>();
	private ArrayList<Node> nodesList = new ArrayList<Node>();
	private ArrayList<Node> queue = new ArrayList<Node>();
	double hypot;
	public FaceMap(){
		
	}
	public void addNode(Node toAdd){
		nodesList.add(toAdd);
	}

	public void addEdge(Edge toAdd){
		edgesList.add(toAdd);
	}
	public Node pop(){
		return queue.remove(0);
	}
	public void push(Node toAdd){
		queue.add(toAdd);
	}
	public void createFaceMap(){
		ArrayList<Node> unvisited = nodesList; //Possible bug in the future
		double minDis = Double.POSITIVE_INFINITY;
		Node minimum = new Node();
		for(int i = 0; i < nodesList.size(); i++){
			double nearO = Math.sqrt(Math.pow(nodesList.get(i).getX(), 2.0) + Math.pow(nodesList.get(i).getY(), 2.0));
			if(nearO < minDis){
				minDis = hypot;	
				minimum = nodesList.get(i);
			}
		}
		
		/*Breadth first search with each node having a max of 3 outdegree edges.*/
		
		while(unvisited.size() != 0){
			if(unvisited.contains(minimum))
				unvisited.remove(minimum); //another possible bug.
			Node nearest = findNearestNode(minimum, unvisited);
			push(nearest);
			edgesList.add(new Edge(minimum, nearest, hypot));
			unvisited.remove(nearest);
			hypot = 0;
			if(unvisited.size() > 0){
				Node nearest2 = findNearestNode(minimum, unvisited);
				push(nearest2);
				edgesList.add(new Edge(minimum, nearest2, hypot));
				unvisited.remove(nearest2);
				hypot = 0;
				if(unvisited.size() > 0){
					Node nearest3 = findNearestNode(minimum, unvisited);
					push(nearest3);
					edgesList.add(new Edge(minimum, nearest3, hypot));
					unvisited.remove(nearest3);
					hypot = 0;
				}
			}
			minimum = pop();
		}
	}
	private Node findNearestNode(Node current, ArrayList<Node> unvisited){
		double minDis = Double.POSITIVE_INFINITY;
		Node minimum = new Node();
		for(int i = 0; i < unvisited.size(); i++){
			hypot = Math.sqrt(Math.pow(nodesList.get(i).getX()-current.getX(), 2.0) + Math.pow(nodesList.get(i).getY()-current.getY(), 2.0));
			if(hypot < minDis){
				minDis = hypot;	
				minimum = nodesList.get(i);
			}
		}
		return minimum;
	}
	
	
}
